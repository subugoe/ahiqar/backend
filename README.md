# Ahiqar Backend

This aims to serve the complete backend for the Ahiqar project.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Ahiqar Backend](#ahiqar-backend)
  - [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Step by step](#step-by-step)
      - [Backend: eXist-db App and Dependencies](#backend-exist-db-app-and-dependencies)
      - [Frontend: Build the docker image](#frontend-build-the-docker-image)
      - [Environment Variables](#environment-variables)
      - [Log in to the Registry](#log-in-to-the-registry)
  - [Architecture](#architecture)
    - [Internal Workings of the Backend](#internal-workings-of-the-backend)
  - [Connecting the Backend with the Frontend](#connecting-the-backend-with-the-frontend)
  - [Tests](#tests)
  - [CI features](#ci-features)
    - [Secrets](#secrets)
    - [Keywords](#keywords)
      - [`ci-reimport-data`](#ci-reimport-data)
  - [API documentation](#api-documentation)
    - [Interplay of TextAPI and AnnotationAPI](#interplay-of-textapi-and-annotationapi)
  - [License](#license)
  - [Contributing](#contributing)
  - [Versioning](#versioning)
  - [Authors](#authors)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Getting Started

For a quick start of the development environment the `./local-cluster-start.sh` is used. The script
will check for all dependencies and complains on missing ones. It will build and install all images
to a local kubernetes cluster set up with `k3d`.

For building the image access to harbor.gwdg.de/sub is required, if the base images are not present
yet. Therefore the script will prompt a ⚠️ warning on checking `~/.docker/config.json` but will
fail on image build afterwards. To obtain the credentials, create your own "Robot Account"
[here](https://harbor.gwdg.de/harbor/projects/10/robot-account). If access is denied, ask the team to
become a member of the project.

### Prerequisites

Please make sure you have the following software installed before building the
backend:

- ant
- k3d
  - kubectl
  - docker
  - helm

The following programs/commands are used, but usually preinstalled with your Linux distribution and corresponding shell:

- bash
- curl
- echo
- mv
- rm
- touch
- unzip

`git clone` related repos to the parent dir. You MUST have the following
structure to run `./local-cluster-start.sh`.

```bash
.
├── backend
├── backend-helm
└── website
```

### Step by Step

While running the script `./local-cluster-start.sh` is sufficient to get
started as it performes all of the following tasks, more details are
provided below.

#### Backend: eXist-db App and Dependencies

```bash
# load eXist-db dependencies (mainly SADE)
ant -f exist-app/build.xml dependencies

# build eXist-db package for Ahiqar
ant -f exist-app/build.xml xar

# optionally in one line
# ant -f exist-app/build.xml dependencies xar
```

The application has to be packed to a docker image with the Dockerfile in the root directory.

The base image (harbor.gwdg.de/sub/existdb@sha256:7837484…) was re-tagged to omit docker login hassle. The main issue is that a docker login is bound to the domain so we cannot login to
two distinct projects in harbor. If an update of the base image is available, we have to re-tag again.

#### Frontend: Build the Docker Image

The website integrating TIDO-Viewer has to be built as Docker image.

See notes in the [corresponding repository](https://gitlab.gwdg.de/subugoe/ahiqar/website).

#### Environment Variables

The deployment will read all secrets from Vault. In order to have a local setup without Vault,
we use environment variables for the created containers. A special kubernetes resources
will be added to the helm chart with the `local-cluster-start.sh` (`-f local-cluster-start-helm-secrets.yaml`).
Variables expected there:

| KEY | VALUE |
|---|---|
| TGLOGIN | colon separated username and password for TextGrid |
| APP_DEPLOY_TOKEN | secret token to start tasks via API |
| EXIST_ADMIN_PW_RIPEMD160 | exist password hash |
| EXIST_ADMIN_PW_DIGEST | exist password digest |

#### Log in to the Registry

In order to pull the required images you have to log in to GWDG's Harbor as well as GitLab registries.

```bash
docker login harbor.gwdg.de/sub/
```

You are then prompted to enter a username and a password. You should use the respective machine / robot user credentials for this.

## Architecture

### Internal Workings of the Backend

![Diagram of the backend's architecture](exist-app/data/ahiqar_backend_architecture.png)

## Connecting the Backend with the Frontend

The corresponding frontend for the Ahiqar backend is [Ahiqar's version of the TIDO viewer](https://gitlab.gwdg.de/subugoe/ahiqar/ahiqar-tido).
In order to connect the back end with the viewer, the former simply has to expose a REST API that complies to the specification of the [SUB's generic TextAPI](https://subugoe.pages.gwdg.de/emo/text-api/).
The specification of the project specific API can be found at the [API's documentation page](https://subugoe.pages.gwdg.de/ahiqar/api-documentation/).

The frontend takes care of the data transfer as described in [TIDO's README](https://gitlab.gwdg.de/subugoe/emo/tido/-/blob/develop/README.md#connecting-the-viewer-with-a-backend).

## Tests

All functions are based on unit tests which are executed automatically as the first stage of this repo's pipelines.

To enable local testing without credentials some tests that require access to TextGrid have been outsourced to separate files in `exist-app/tests`.
These are only executed by the `testtrigger.xqm` module if the respective environment variable, `TGLOGIN` is set in `ahiqar.env`.

This way, tests can be executed locally via the API endpoint `/trigger-tests` or the script `tests-runner.xq` without running into authentication problems.

## CI features

GitLab CI will build, test and deploy everything.
It creates new Docker images to be deployed to the GitLab Container Registry.
It will push the XAR package (eXist app) to the [DARIAH-DE eXist repo](https://ci.de.dariah.eu/exist-repo)
It then triggers all updates on the server.
This is done for all instances (test, dev, main).

### Secrets

Secrets are managed externally by [Vault](https://secs.sub.uni-goettingen.de). During the CI pipeline the env file is prepared and copied over to the server. This implementation is very basic
and changes to the secrets are not considered by following workflows. Changing a secret in the secret store will not cause an update server-side.

### Keywords

#### `ci-reimport-data`

- removes the docker volume on the adressed instance (test, dev, main)
- will add a job to the pipeline for reimporting data to the adressed instance (test, dev, main)

## API documentation

The backend comes shipped with an OpenAPI documentation of its API.
The docs are available at <https://ahiqar-dev.sub.uni-goettingen.de/openapi> or at <https://subugoe.pages.gwdg.de/ahiqar/api-documentation/>.

### Interplay of TextAPI and AnnotationAPI

![Diagram of the interplay of TextAPI and AnnotationAPI](exist-app/data/annotationAPI.png)

## License

See the [LICENSE file](LICENSE.md) for more information on how to re-use this software.

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](https://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.gwdg.de/subugoe/ahiqar/backend/-/tags).

## Authors

- [Mathias Göbel](https://gitlab.gwdg.de/mgoebel)
- [Michelle Weidling](https://gitlab.gwdg.de/mrodzis)

See also the list of contributors who participated in this project.

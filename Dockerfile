FROM harbor.gwdg.de/sub/existdb:6.3.0

# copy EXPath packages
ARG XAR_FILE="exist-app/build/*.xar"
COPY ${XAR_FILE} /exist/autodeploy/
COPY exist-app/build/dependencies/*.xar /exist/autodeploy/

# copy custom config
# COPY exist-conf.xml /exist/etc/conf.xml
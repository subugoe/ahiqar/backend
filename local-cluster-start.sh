#!/bin/bash
# SPDX-FileCopyrightText: 2024 Georg-August Universität Göttingen Stiftung öffentlichen Rechts
# SPDX-License-Identifier: Beerware
#
# cluster start script for doing literally everything
# This script sets up a complete kubernetes installation for Ahiqar including
# the following steps:
#   1. build eXist application (xar package)
#   2. build container image including the xar package and all dependencies
#      optionally build the website container image as well
#   3. create a fresh local kubernetes cluster with k3d
#   4. adjust the cluster with nginx-ingress and import the prepared container image
#   5. set all references (secrets, image tags) and install the helm chart

# parsing incoming parameter, see: https://stackoverflow.com/a/14203146
POSITIONAL_ARGS=()
while [[ $# -gt 0 ]]; do
  case $1 in
    -p|--port-forward)
      PORT="$2"
      shift # past argument
      shift # past value
      ;;
    --debug)
      DEBUG=true
      printf "[DEBUG] enabled\n"
      shift # past argument
      ;;
    --slow)
      TIMEOUT="420s"
      shift
      ;;
    --help)
      printf ' 🚀 starting your cluster!\nparameters:\n  -p --port-forward [NUM] if set, a kubectl port-forward to existdb-0 will be started at the end.\n  --slow additional time for all timeouts\n  -w --build-website build an image for the website.\n  --debug increase verbosity to the max\n'
      exit 0
      ;;
    -w|--build-website)
      BUILD_WEBSITE=true
      shift # past argument
      ;;
    -*|--*)
      printf " 🧨\tUnknown option $1\n\trun with --help for more usage instructions"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done

# end script on failing command (exit code)
set -e

_PWD=$(pwd)
epoch=$(date +%s)
NAME="ahiqar"

if [ "${DEBUG}" == "true" ]; then
  printf "[DEBUG] NAME=${NAME}\n"
  printf "[DEBUG] epoch=${epoch}\n"
  ANT_OPT="-verbose"
  ANT_OUT="/dev/stdout"
  DOCKER_OPT="--progress plain"
  DOCKER_OUT="/dev/stdout"
  HELM_OPT="--debug"
  HELM_OUT="/dev/stdout"
  K3D_OPT="--verbose"
  K3D_OUT="/dev/stdout"
  KUBECTL_OUT="/dev/stdout"
else
  ANT_OPT="-silent"
  ANT_OUT="/dev/null"
  DOCKER_OPT="--quiet"
  DOCKER_OUT="/dev/null"
  HELM_OUT="/dev/null"
  K3D_OPT=" "
  K3D_OUT="/dev/null"
  KUBECTL_OUT="/dev/null"
fi

[ -z "${TIMEOUT}" ] && TIMEOUT="200s"

# checking for dependencies
printf " ⚙️\ttesting for dependencies "
which k3d > /dev/null
which kubectl > /dev/null
which jq > /dev/null
which helm > /dev/null

if ! which ant > /dev/null ; then printf " ⚠️\tno local installation of ant present. using docker image instead."; fi

if [ ! -d ../backend-helm ] || [ ! -d ../website ] ; then
  printf "\n 🧨\tplease add ../backend-helm and ../website"
  exit 1
fi

# checking for local config
if ! grep --quiet "${NAME}.local" /etc/hosts   ; then printf " ⚠️\t${NAME}.local not found in /etc/hosts. please add the following line\n\t127.0.0.1 ahiqar.local api.ahiqar.local\n" && exit 1; fi
if ! grep --quiet "api.${NAME}.local" /etc/hosts; then printf " ⚠️\tapi.${NAME}.local not found in /etc/hosts. please add the following line\n\t127.0.0.1 ahiqar.local api.ahiqar.local\n" && exit 1; fi
[[ ! -f local-cluster-start-helm-secrets.yaml ]] && (printf " ⚠️\tplease add valid credentials to local-cluster-start-helm-secrets.yaml\n" & \
  printf 'existdb:
  image:
    tag: "local-test"
  env:
    - name: TGLOGIN
      value: "TextGridUsername:TextGridPassword"
    - name: "EXIST_ADMIN_PW_RIPEMD160"
      value: "{RIPEMD160}nBGFpcXp/FRhKAiXfuj1SLIljTE="
    - name: EXIST_ADMIN_PW_DIGEST
      value: 91f9f01546c21c55bd4f8f122673a84856a71a1e
    - name: EXIST_ADMIN_PW
      value: ""
    - name: APP_DEPLOY_TOKEN
      value: "1234"' \
  > local-cluster-start-helm-secrets.yaml && exit 1)

DOCKER_BASE_IMAGE=$(grep -e "^FROM" Dockerfile | cut -f 2 -d " ")
if [ -z "$(docker images -q ${DOCKER_BASE_IMAGE} 2> /dev/null)" ]; then
  printf "🐋\tbase image not in local docker image cache: ${DOCKER_BASE_IMAGE}"
  if [ ! "$(jq -r '.auths."harbor.gwdg.de".auth' ~/.docker/config.json | base64 -d | cut -f 1 -d '+')" == 'robot$sub' ] ; then
    printf "⚠️\tadd credentials for harbor.gwdg.de/sub to your docker config. for details see: https://harbor.gwdg.de/harbor/projects/10/robot-account";
    exit 11
  fi
fi

printf "\r ✅\tall tests for dependencies have been passed\n"

# build exist app
printf " ⚙️\tbuild XAR package "
if which ant &> /dev/null ; then
  printf " with ant"
  cd exist-app
  ant ${ANT_OPT} dependencies xar > ${ANT_OUT}
  cd ${_PWD}
else
  printf " with 🐋 harbor.gwdg.de/sub-fe-pub/ant"
  cd exist-app
  docker run -it -v .:/data:z -w /data --user "${UID}:${GID}" harbor.gwdg.de/sub-fe-pub/ant:openjdk-slim-bookworm-ant1.10.13 ant ${ANT_OPT} dependencies xar > ${ANT_OUT}
  cd ${_PWD}
fi
XAR_FILE=$(ls exist-app/build/*.xar -tr | tail -1)
printf "\r ✅\tcreated XAR package ${XAR_FILE}\n"

# build docker image
## it is required to have a single xar package in the autodeploy directory
printf " ⚙️\tbuild docker images "
docker build \
  ${DOCKER_OPT} \
  --file Dockerfile \
  --build-arg="XAR_FILE=${XAR_FILE}" \
  --tag harbor.gwdg.de/sub-fe/subugoe/ahiqar/backend/existdb:local-test-${epoch} \
  . 1> ${DOCKER_OUT}

# and also build the website on request…
if [ "${BUILD_WEBSITE}" == "true" ]; then
  docker build \
    ${DOCKER_OPT} \
    --file ../website/Dockerfile \
    --tag harbor.gwdg.de/sub/subugoe/ahiqar/website:local-test-${epoch} \
    ../website 1> ${DOCKER_OUT}
else # …or check for the image being present
  WEBSITE_IMAGE="harbor.gwdg.de/sub/subugoe/ahiqar/website@sha256:4dbd34adbd2ae0bb38f7ad9ac5e603b37496d11a76a53ad4bafca5f783e4fb2d"
  if [ -z "$(docker images -q ${WEBSITE_IMAGE} 2> /dev/null)" ]; then
    printf "Dockerfile: website image not in local cache\n"
    if [ ! "$(jq -r '.auths."harbor.gwdg.de".auth' ~/.docker/config.json | base64 -d | cut -f 1 -d '+')" == 'robot$sub' ] ; then
      printf " ⚠️\tadd credentials for harbor.gwdg.de/sub to your docker config. for details see: https://harbor.gwdg.de/harbor/projects/10/robot-account\n";
      exit 11
    fi
    docker pull ${WEBSITE_IMAGE}
    docker tag ${WEBSITE_IMAGE} harbor.gwdg.de/sub/subugoe/ahiqar/website:local-test-${epoch}
  fi
  docker tag ${WEBSITE_IMAGE} harbor.gwdg.de/sub/subugoe/ahiqar/website:local-test-${epoch}
fi
printf "\r ✅\tall docker images have been created\n ⚙️\tpreparing a clean cluster "

# remove olde cluster
[ "${DEBUG}" == "true" ] && printf "k3d cluster delete ${NAME} ${K3D_OPT} 1> ${K3D_OUT}\n"
k3d cluster delete ${NAME} ${K3D_OPT} 1> ${K3D_OUT}
[ "${DEBUG}" == "true" ] && printf "k3d cluster create ${NAME} --k3s-arg \"--disable=traefik@server:0\" -p \"80:80@loadbalancer\" ${K3D_OPT} 1> ${K3D_OUT}\n"
k3d cluster create ${NAME} --k3s-arg "--disable=traefik@server:0" -p "80:80@loadbalancer" ${K3D_OPT} 1> ${K3D_OUT}

# import all images needed
k3d image import --cluster ${NAME} \
    harbor.gwdg.de/sub/subugoe/ahiqar/website:local-test-${epoch} \
    harbor.gwdg.de/sub-fe/subugoe/ahiqar/backend/existdb:local-test-${epoch} \
    registry.k8s.io/ingress-nginx/controller:v1.10.1 \
    ${K3D_OPT} 1> ${K3D_OUT}

# wait for the metrics server to be ready, preventing E0522
[ "${DEBUG}" == "true" ] && printf "kubectl wait for kube-system\n"
kubectl --namespace kube-system wait --for=condition=Ready pods --all --timeout=${TIMEOUT} &> ${KUBECTL_OUT}
[ "${DEBUG}" == "true" ] && printf "✅ kubectl wait for kube-system\n"

# add nginx-ingress controller (chart version: 4.10.0 → nginx version 1.10.0)
helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace kube-system \
  --set controller.enableSnippets="true"\
  ${HELM_OPT} 1> ${HELM_OUT}

# wait for nginx service to become available
[ "${DEBUG}" == "true" ] && printf " 🐸\twaiting for ingress-nginx to become available\n"
kubectl --namespace kube-system wait deployments.apps ingress-nginx-controller --for=jsonpath='{.status.readyReplicas}'=1 --timeout=${TIMEOUT} 1> ${KUBECTL_OUT}
[ "${DEBUG}" == "true" ] && printf " 🐸\tingress-nginx is available now\n"

kubectl label ingressclasses.networking.k8s.io nginx ingressclass.kubernetes.io/is-default-class="true" 1> ${KUBECTL_OUT}

# install our chart
helm install ${NAME} ../backend-helm/backend/ \
  --namespace ${NAME} --create-namespace \
  --render-subchart-notes -f local-cluster-start-helm-secrets.yaml \
  --set "existdb.image.tag=local-test-${epoch}" \
  --set "website.image.repository=harbor.gwdg.de/sub/subugoe/ahiqar/website" \
  --set "website.image.tag=local-test-${epoch}" \
  ${HELM_OPT} 1> ${HELM_OUT}

kubectl config set-context --current --namespace="${NAME}" 1> ${KUBECTL_OUT}

# here we can utilize kubectl wait as well
printf " …fast geschafft: waiting for pods/ahiqar-existdb-0 to become ready"
kubectl wait --for=condition=Ready pods/ahiqar-existdb-0 --timeout=${TIMEOUT} 1> ${KUBECTL_OUT}

until [[ $(curl --head --silent --output /dev/null --write-out "%{http_code}" http://api.ahiqar.local/info) == "200" ]]; do
  printf "…"
  sleep 1
done && printf "\33[2K\r 🚀\tcluster is ready and helm charts are installed\n"

#curl --silent --user "admin:" http://api.ahiqar.local/exist/apps/ahiqar/tests/tests-runner.xq

printf " ⚙️\tuploading some TEI files "
for i in exist-app/data/*teixml.xml ; do
  filename=$(cut -d / -f 3 <<<"${i}")
  curl --user "admin:" -X PUT --upload-file ${i} -H "Content-Type: application/xml" http://api.ahiqar.local/exist/rest/db/data/textgrid/data/${filename}
done

printf '\33[2K\r ✅\tuploaded some TEI files\n
   a possible next step is to import all data
   connect to the pod with a port-forward (as the restxq servlet is exposed only)
   kubectl port-forward ahiqar-existdb-0 ${PORT}:8080 & \
   curl --user "admin:" http://localhost:${PORT}/exist/apps/ahiqar/import-data.xq?cleanup=true

   or you might want to run some nice tests with
   curl --user "admin:" http://api.ahiqar.local/trigger-unit-tests\n'

if [ ! -z ${PORT} ]; then
  kubectl port-forward ahiqar-existdb-0 ${PORT}:8080 1> /dev/null &
  printf '\nPort-forward active in the background mapping to ${PORT}
  While no other kubectl process is running, stop port-forward with \"pkill kubectl -9\"
  Restarting this script will automatically remove the connection and prints \"error: lost connection to pod\" when deleting the cluster.\n'
fi

exit 0
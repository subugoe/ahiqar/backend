#!/bin/bash
# load all transformation results from service (mainly the GitLab CI job's service container)
# PUT the results to a target instance

# parsing incoming parameter, see: https://stackoverflow.com/a/14203146
POSITIONAL_ARGS=()
while [[ $# -gt 0 ]]; do
  case $1 in
    -s|--service)
      SERVICE="$2"
      REST="${SERVICE}/exist/rest/data/textgrid"
      shift # past argument
      shift # past value
      ;;
    -uri|--textgrid-uri)
      TG_URI="$2"
      shift # past argument
      shift # past value
      ;;
    -t|--target)
      TARGET="$2"
      shift # past argument
      shift # past value
      ;;
    -p|--target-password)
      EXIST_ADMIN_PW="$2"
      shift # past argument
      shift # past value
      ;;
    --help)
      printf ' 🚀 import a textgrid edition and pass all import artifacts to a target instance \n\n parameter:\n -s|--service where to start the import\n -uri|--textgrid-uri the object to import recursively\n -t|--target where to PUT the data\n -p|--target-password the admin password for the target instance'
      exit 0
      ;;
    -*|--*)
      printf " 🧨\tUnknown option $1\n\trun with --help for more usage instructions"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done

# end script on failing command (exit code)
set -e

TOKEN="${APP_DEPLOY_TOKEN:-1234}"

printf " ⏱️ waiting for service to be up and running…"
until (curl -I -s "${SERVICE}"); do printf "…" && sleep 1; done
curl --silent "${SERVICE}/exist/restxq/import-data/${TG_URI}?token=${TOKEN}&cleanup=false&recursive=true"

# since the service starts with empty transformation results collections, we can load all resources

# prepare
TMPDIR="./export/${TG_URI/textgrid:/}"
mkdir --parents ${TMPDIR}
cd ${TMPDIR} && echo ${PWD}

# load all resources form tmp, data and html
COLLECTIONS=( "agg" "data" "html" "meta" "json" "tmp" )
for collection in ${COLLECTIONS[@]}; do
    mkdir ${collection}
    for i in $(curl --silent ${REST}/${collection} | xmlstarlet sel -t -v "//exist:resource/@name"); do
    curl --silent ${REST}/${collection}/${i} -o ${collection}/${i}
    done
done

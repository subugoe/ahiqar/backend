import json
import sys
import urllib.parse

import requests

BRANCH = sys.argv[1]

match BRANCH:
    case 'main':
        SERVER = 'https://ahiqar.uni-goettingen.de/'
    case 'develop':
        SERVER = 'https://ahikar-dev.sub.uni-goettingen.de/'
    case _:
        SERVER = 'https://ahikar-test.sub.uni-goettingen.de/'

ARA_URL = f'{SERVER}api/textapi/ahiqar/arabic-karshuni/collection.json'
SYR_URL = f'{SERVER}api/textapi/ahiqar/syriac/collection.json'

def walk_sequence(sequence: list) -> None:
    """
    Recursively iterates over the sequence items (if present).
    """
    for item in sequence:
        url = item['id']
        check_url(url)

def check_url(url: str) -> None:
    """
    Checks the status code of a given URL as well as HTML pages (if present).
    Recursively iterates over the sequence items (if present).
    """
    req = requests.get(url, timeout=15)
    status = req.status_code

    if status == 200:
        result[status].append(url)
        validate(url)
        content = req.text
        json_data = json.loads(content)

        try:
            check_html(json_data['content'][0]['url'])
        except KeyError:
            pass

        try:
            walk_sequence(json_data['sequence'])
        except KeyError:
            pass

        if 'item.json' in url:
            check_annotations(json_data['annotationCollection'])
            check_image(json_data['image']['id'])
    else:
        result[status].append({url: req.text})


def check_html(url: str) -> None :
    """
    Investigates the HTTP status code of the rendered HTML files.
    """
    req = requests.get(url, timeout=15)
    status = req.status_code
    result[status].append(url)

def check_annotations(url: str) -> None:
    req = requests.get(url, timeout=15)
    status = req.status_code

    if status == 200:
        result[status].append(url)
        content = req.text
        json_data = json.loads(content)
        check_annotations(json_data['first'])
    else:
        result[status].append({url: req.text})

def check_image(url: str) -> None:
    req = requests.get(url, timeout=15)
    status = req.status_code

    if status == 200:
        result[status].append(url)
    else:
        result[status].append({url: req.text})

def validate(url: str) -> None:
    validation_url = 'https://validation.textapi.sub.uni-goettingen.de/'
    encoded = urllib.parse.quote(url, safe='')
    request_url = f'{validation_url}?url={encoded}&recursive=false'
    req = requests.post(request_url, timeout=15)
    status = req.status_code

    if not status in [200, 204]:
        result['textapi_invalid'][status].append({url: req.text})

result = {
            200: [],
            404: [],
            500: [],
            'textapi_invalid': 
                {
                    422: [],
                    500: []
                }
          }

print('Starting to traverse the complete Ahiqar TextAPI. This will take some time.')

for coll in [SYR_URL, ARA_URL]:
    check_url(coll)

with open('ahiqar-textapi-result.json', encoding='utf-8', mode='w') as f:
    f.write(json.dumps(result))

print('...oooOOO___ Done! ___OOOooo...')
#!/bin/bash

# deploy app by API call
# to be executed server side after docker-compose up

VERSION=$1
CI_COMMIT_REF_NAME=$2

case ${CI_COMMIT_REF_NAME} in
    "main")
    source ${CI_COMMIT_REF_NAME}/ahiqar.env
    URL="http://localhost:8092/api/deploy?token=${APP_DEPLOY_TOKEN}&version=${VERSION}"
    ;;
    "develop")
    source ${CI_COMMIT_REF_NAME}/ahiqar.env
    URL="http://localhost:8093/api/deploy?token=${APP_DEPLOY_TOKEN}&version=${VERSION}"
    ;;
    *)
    source test/ahiqar.env
    URL="http://localhost:8094/api/deploy?token=${APP_DEPLOY_TOKEN}&version=${VERSION}"
    ;;
esac

HEADER=$(curl --head -s ${URL})

STATUS=$(echo $HEADER | head -n 1 | cut -d" " -f 2)
echo "Current HTTP status is $STATUS."

if [[ "$STATUS" != "200" ]]; then
    curl -vvv ${URL} > curl.log
    head -n 30 curl.log
    exit 1
fi

#!/bin/bash

# quick test for existdb beeing available
# to be executed server side after docker-compose up

CI_COMMIT_REF_NAME=$1


case ${CI_COMMIT_REF_NAME} in
    "main")
    URL="http://localhost:8092/rest/"
    ;;
    "develop")
    URL="http://localhost:8093/rest/"
    ;;
    *)
    URL="http://localhost:8094/rest/"
    ;;
esac

while [ $(curl --head --silent $URL | grep -c "200 OK") -ne 1 ]; do
    echo "$(date) existdb at $URL not ready"
    sleep 3s
done

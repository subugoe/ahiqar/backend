#!/bin/bash

# quick test for existdb apis availability
# to be executed server side after docker-compose up

CI_COMMIT_REF_NAME=$1

case ${CI_COMMIT_REF_NAME} in
    "main")
    source ${CI_COMMIT_REF_NAME}/ahiqar.env
    URL="http://localhost:8092/api/import-data-all?token=${APP_DEPLOY_TOKEN}"
    ;;
    "develop")
    source ${CI_COMMIT_REF_NAME}/ahiqar.env
    URL="http://localhost:8093/api/import-data-all?token=${APP_DEPLOY_TOKEN}"
    ;;
    *)
    source test/ahiqar.env
    URL="http://localhost:8094/api/import-data-all?token=${APP_DEPLOY_TOKEN}"
    ;;
esac

echo "$(date) ::: performing data import via ${URL}"
echo "server-side this will trigger a exerr:ERROR Unable to encode string value: Underlying channel has been closed."

header=$(curl --head -s --max-time 180 ${URL})

echo "$(date) ::: done. we do not check for the operation beeing completed, but we are sure the operation started!"

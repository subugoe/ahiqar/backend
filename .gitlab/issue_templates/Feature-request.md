# Feature Request

## Description

## User Story

As (role of the user) I need (what has to be done) in order to (what I want to achieve with this feature).

/cc [Mathias Göbel](https://gitlab.gwdg.de/mgoebel), [Frank Schneider](https://gitlab.gwdg.de/schneider210), [Michelle Weidling](https://gitlab.gwdg.de/mrodzis)

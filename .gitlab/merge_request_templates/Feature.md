# Feature

## Summary

This MR provides 

## Compliance to “Definition of Done”

* [ ] Unit tests passed
* [ ] Code reviewed
* [ ] Product Owner accepts the User Story

## Documentation

* [ ] I updated the README (if applicable)
* [ ] I provided my functions with appropriate documentation
* [ ] I updated existing documentation

## Tests

Are we able to test this new feature?

* [ ] Yes, everything can be done via unit tests.
* [ ] Yes, you can test by following these steps: …
* [ ] No, it is not possible.

## Changelog

* [ ] I added a statement to the CHANGELOG.

## Version number

* [ ] I bumped the version number in `build.properties`.

## Closes

Closes

## Logs and Screenshots

/cc [Mathias Göbel](https://gitlab.gwdg.de/mgoebel), [Frank Schneider](https://gitlab.gwdg.de/schneider210), [Michelle Weidling](https://gitlab.gwdg.de/mrodzis)

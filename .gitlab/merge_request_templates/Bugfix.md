# Bug fix

## Summary

## Compliance to “Definition of Done”

* [ ] Unit tests passed
* [ ] Code reviewed
* [ ] Product Owner accepts the User Story

## Changelog

* [ ] I added a statement to the CHANGELOG.

## Version number

* [ ] I bumped the version number in `build.properties`.

## Readme and general docs

* [ ] I updated the README (if applicable) and kept the docs up to date.

## Closes

/cc [Mathias Göbel](https://gitlab.gwdg.de/mgoebel), [Frank Schneider](https://gitlab.gwdg.de/schneider210), [Michelle Weidling](https://gitlab.gwdg.de/mrodzis)

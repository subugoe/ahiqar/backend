xquery version "3.1";

(:~
 : This module provides the editorial annotations for Ahiqar.
 :
 : @author Michelle Weidling
 : @version 1.9.0
 : @since 1.7.0
 : @see https://subugoe.pages.gwdg.de/ahiqar/api-documentation/
 : @see https://subugoe.pages.gwdg.de/ahiqar/api-documentation/page/text-api-specs/
 : :)

module namespace edit="http://ahiqar.uni-goettingen.de/ns/annotations/editorial";

declare namespace tei="http://www.tei-c.org/ns/1.0";

import module namespace commons="http://ahiqar.uni-goettingen.de/ns/commons" at "../commons.xqm";

(:~
 : Gets the annotations for a given page for both possible text types
 : (transcription and transliteration, if present).
 : 
 : At this stage, TEI files are scraped for person and place names.
 : 
 : @param $teixml-uri The XML's URI.
 : @param $page The page within an XML file, i.e. a tei:pb/@n within a TEI resource
 :)
declare function edit:get-annotations($pages as element(tei:TEI)+,
    $teixml-uri as xs:string)
as map()* {
    let $annotation-elements := 
        for $chunk in $pages return
            $chunk//(
                tei:placeName |
                tei:persName |
                tei:add |
                tei:del |
                tei:damage |
                tei:choice[not(parent::tei:damage)] |
                tei:unclear |
                tei:cit[not(@next)] | (: the cit elements that point to another have no original text in them and are therefore ignored :)
                tei:surplus |
                tei:seg[@type = "verse"] |
                tei:catchwords
               (:  | tei:quote  # remove quotations :)
            )
    
    for $annotation in $annotation-elements
        let $map := edit:make-map($annotation, $teixml-uri)
        let $value := string($map("body")("value"))
        (: remove empty value annotations :)
        where $value ne ""
        return
            $map
};

declare function edit:make-map($annotation as element(),
    $teixml-uri as xs:string)
as map(*) {
    let $id := string( $annotation/@id ) (: get the predefined ID from the in-memory TEI with IDs :)
    return
        map {
            "id": $commons:anno-ns || "/" || $teixml-uri || "/annotation-" || $id,
            "type": "Annotation",
            "body": edit:get-body-object($annotation),
            "target": array { edit:get-target-information($annotation, $teixml-uri, $id) }
        }
};


(:~
 : Returns the Body Object for an annotation.
 : 
 : @see https://www.w3.org/TR/annotation-model/#embedded-textual-body
 : @see https://subugoe.pages.gwdg.de/ahiqar/api-documentation/page/annotation-api-specs/#body-object
 : 
 : @param $annotation The node which serves as a basis for the annotation
 : @return A map representing the embedded textual body of the annotation.
 :)
declare function edit:get-body-object($annotation as node())
as map() {
    map {
        "type": "TextualBody",
        "value": edit:get-body-value($annotation),
        "format": "text/plain",
        "x-content-type": edit:get-annotation-type($annotation)
    }
};

declare function edit:make-annotation-text($annotation as node())
as xs:string? {
    (: annotations with @next are the first part of a name spanning over
    two lines. :)
    if ($annotation/@next) then
        let $first-part-text := $annotation//text() => string-join(" ")
        let $second-part-text := $annotation/id(substring-after($annotation/@next, "#"))//text() => string-join(" ")
        return $first-part-text || $second-part-text
    (: annotations with @xml:id are the second part of a name that is split
    over two lines. the second part has already been handled by the first
    part and can be savely ignored. :)
    else if ($annotation/@xml:id) then
        ()
    else
        $annotation//text() => string-join(" ")
};

(:~
 : Returns the body's value for an annotation.
 : 
 : @see https://www.w3.org/TR/annotation-model/#embedded-textual-body
 : 
 : @param $annotation The node which serves as a basis for the annotation
 : @return The value of the annotation.
 :)
declare function edit:get-body-value($annotation as node())
as xs:string {
    let $value :=
        typeswitch ( $annotation )
        case element(tei:persName) return
            edit:make-annotation-text($annotation)

        case element(tei:placeName) return
            edit:make-annotation-text($annotation)
            
        case element(tei:add) return
            "an addition, place: " || $annotation/@place || ". text: " || $annotation//text() => string-join(" ") || "."
            
        case element(tei:del) return
            "text deleted by the scribe: " || $annotation//text() => string-join(" ")
            
        case element(tei:damage) return
            if ($annotation/tei:choice/tei:orig and $annotation/tei:choice/tei:supplied) then
                "a damaged passage. original: " || $annotation//tei:orig || ", supplied text: " || $annotation//tei:supplied
            else if ($annotation/tei:choice/tei:orig and $annotation/tei:choice/tei:corr) then
                "a damaged passage. original: " || $annotation//tei:orig || ", corrected text: " || $annotation//tei:corr
            else if ($annotation/text()[matches(., "[\w]")]) then
                "a damaged passage. legible text: " || $annotation
            else if ($annotation/tei:g) then
                if ($annotation/tei:g[@type = "quotation-mark"]) then
                    "a damaged passage. legible text: quotation mark"
                else
                    ()
            else
                let $text := string-join($annotation/descendant::text(), " ")
                return
                    "a damaged passage. legible text: " || $text

        case element(tei:choice) return
            if($annotation/tei:sic) then
                let $resp :=
                    if ($annotation/tei:corr/@resp) then
                        "by the editor"
                    else
                        "by the scribe"
                return
                    "correction of faulty text. original: " || $annotation/tei:sic => string-join(" ") || 
                    ", corrected " || $resp || " to: " || $annotation/tei:corr//text() => string-join(" ")
            else
                "an abbreviation. original: " || $annotation/tei:abbr => string-join(" ") || ", expanded text: " || $annotation/tei:expan => string-join(" ")
                
        case element(tei:unclear) return
            if ($annotation/@reason) then
                "a passage where the writing cannot be fully deciphered. text: " || $annotation || ", reason: " || replace($annotation/@reason, "_", " ")
            else
                "a passage where the writing cannot be fully deciphered. text: " || $annotation
        
        case element(tei:cit) return
            if ($annotation/@type = 'verbatim') then
                string-join($annotation//text(), " ") || ": a quote of " || ($annotation/tei:bibl => string-join(" "))
                
            else if ($annotation/@type = "paraphrase") then
                edit:make-paraphrase-text($annotation)

            else
                let $references-strings :=
                    for $note in $annotation/tei:note return
                        (($note/following-sibling::tei:bibl[1]//text() => string-join(" "))
                         || ": "
                         || ($note//text() => string-join(" ")))
                return        
                    string-join($references-strings, "; &#10;")
        
        case element(tei:surplus) return
            "surplus text: " || string-join($annotation//text(), " ")

        case element(tei:quote) return
            "quotation: " || string-join($annotation//tei:w, " ")
            
        case element(tei:seg) return
            "inline verse: " || string-join($annotation//tei:w, " ")
            
        case element(tei:catchwords) return
            "catchword(s): " || string-join($annotation//text(), " ")

        default return
            ()

    return
        (: normalize space of an empty sequence will return an empty string! :)
        normalize-space($value)
};


(: Creates the paraphrase text for all kinds of paraphrases (single vs. multiline). :)
declare function edit:make-paraphrase-text($cit as element(tei:cit))
as xs:string? {
    let $referencing-cit := edit:find-referencing-cit($cit)
    let $paraphrase-text :=
        (: multiline paraphrase :)
        if ($referencing-cit) then
            let $text-of-prev-lines := edit:find-prev-paraphrases-texts($cit, $referencing-cit)
            return string-join(($text-of-prev-lines, $cit/tei:quote//text()), " ")
        (: single line paraphrase :)
        else
            $cit/tei:quote//text() => string-join(" ")
    let $orig-phrases := $cit/tei:note[@type = "parallel-text"]/tei:note[@type = "original-phrase"]
    return
        if (count($orig-phrases) = 1) then
            let $orig-phrase := $cit/tei:note[@type = "parallel-text"]/tei:note[@type = "original-phrase"]//text()
            let $orig := $cit/tei:note[@type = "parallel-text"]/tei:bibl[@type = "provided-by-editor"]//text()
            return
                '"' || $paraphrase-text || '": a paraphrase of "' || string-join($orig-phrase, " ") || '" (' || $orig || ')'
        else
            let $origs :=
                for $note in $cit/tei:note[@type = "parallel-text"] return
                    '"' || $note/tei:note//text() || '" (' || $note/tei:bibl//text() || ')'
            return
                '"' || $paraphrase-text || '": a paraphrase of the following parallel texts: ' || string-join($origs, ", ")
};

(: Returns the tei:cit that is referencing another.
 : Relevant for multiline paraphrases. :)
declare function edit:find-referencing-cit($cit as element(tei:cit))
as element(tei:cit)? {
    let $id := $cit/@xml:id
    return $cit/preceding::tei:cit["#" || $id = ./@next]
};

(: In case of a multiline paraphrase, returns all paraphrase texts that don't
 : have original text. :)
declare function edit:find-prev-paraphrases-texts($cit as element(tei:cit),
    $referencing-cit as element(tei:cit))
as xs:string {
    let $next-reference := edit:find-referencing-cit($referencing-cit)
    return
        if ($next-reference) then
            string-join((edit:find-prev-paraphrases-texts($referencing-cit, $next-reference), 
                $referencing-cit/tei:quote//text()), " ")
        else
            $referencing-cit/tei:quote//text() => string-join(" ")
};

(: This function in necessary for getting the correct CSS values. :)
declare function edit:find-prev-paraphrases-ids($cit as element(tei:cit))
as xs:string {
    let $prev-paraphrase-part := edit:find-referencing-cit($cit)
    return
        if ($prev-paraphrase-part) then
            string-join((edit:find-prev-paraphrases-ids($prev-paraphrase-part), "#" || $cit/@id), ",")
        else
            "#" || $cit/@id
};

(:~
 : Returns the type of an annotation.
 : 
 : @see https://www.w3.org/TR/annotation-model/#string-body
 : 
 : @param $annotation The node which serves as a basis for the annotation
 : @return The content of bodyValue.
 :)
declare function edit:get-annotation-type($annotation as node())
as xs:string {
    switch ($annotation/local-name())
        case "persName" return "Person"
        case "placeName" return "Place"
        case "cit" return "Reference"
        default return "Editorial Comment"
};


(:~
 : Returns the target segment for an annotation.
 : 
 : @param $annotation The node which serves as a basis for the annotation
 : @param $document-uri The resource's URI to which the $annotation belongs to
 : @param $id The node ID of the annotation. It is equivalent to generate-id($annotation)
 : @return A map containing the target information
 :)
declare function edit:get-target-information($annotation as node(),
    $document-uri as xs:string,
    $id as xs:string)
as map(*) {
    let $text-type :=
        if ($annotation/ancestor::tei:text[@xml:lang = 'karshuni']) then
            "transliteration"
        else
            "transcription"

    let $page := $annotation/preceding::tei:pb[1]/@n
    let $value := edit:make-css-target-values($annotation)
    return
        map {
        	"selector": map {
                "type": "CssSelector",
                "value": $value
            },
            "source": $commons:server || "/content/" || $text-type || "/" || $document-uri || "-" || $page || ".html",
            "format": "text/xml",
            "language": $annotation/ancestor-or-self::*[@xml:lang][1]/@xml:lang/string()
        }
};

declare function edit:make-css-target-values($annotation as node())
as xs:string? {
    if ($annotation[self::tei:cit[@type = "paraphrase"]]) then
        edit:find-prev-paraphrases-ids($annotation)
    else
        if ($annotation/@next) then
            let $second-part-id := $annotation/following::*["#" || @xml:id = $annotation/@next]/@id
            return
                "#" || $annotation/@id || ",#" || $second-part-id
        else if ($annotation/@xml:id) then
            ()
        else
            "#" || $annotation/@id
};

xquery version "3.1";

(:~
 : This module normalizes Syriac and Arabic text by removing all diacritics from
 : them.
 : 
 : The following Unicode code blocks are sorted out:
 : * Syriac
 :      * Syriac points (vowels)
 : 
 : The vocalization of the Arabic text is kept.
 :
 : @see https://unicode-table.com/en/blocks/arabic/
 : @see https://unicode-table.com/en/blocks/syrian/
 :)

module namespace norm="http://ahiqar.uni-goettingen.de/ns/tapi/txt/normalization";

declare variable $norm:syriac-vowels :=
    (
        1840 to 1855 (: decimal for unicode U+073A and following :)
    );
    
declare variable $norm:syriac-punctuation :=
    (
        1792 to 1806 (: decimal for unicode U+0700 to U+070E :)
    );
    
declare variable $norm:latin-punct :=
    (
        33 to 47
    );

declare variable $norm:combining-marks :=
    (
        768 to 879
    );
    
declare variable $norm:diacritics :=
    (
        $norm:syriac-vowels,
        $norm:syriac-punctuation,
        $norm:latin-punct,
        $norm:combining-marks
    );

declare function norm:get-txt-without-diacritics($txt as xs:string)
as xs:string* {
    string-to-codepoints($txt)
    => norm:remove-codepoints()
    => codepoints-to-string()
};


declare function norm:remove-codepoints($codepoints as xs:integer*)
as xs:integer* {
    for $cp in $codepoints return
        if ($cp = $norm:diacritics)  then
            ()
        else
            $cp
};

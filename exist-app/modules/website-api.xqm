xquery version "3.1";

module namespace website="http://ahiqar.uni-goettingen.de/ns/website";

import module namespace commons="http://ahiqar.uni-goettingen.de/ns/commons" at "commons.xqm";
import module namespace rest="http://exquery.org/ns/restxq";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace xhtml="http://www.w3.org/1999/xhtml";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";

declare
    %rest:GET
    %rest:HEAD
    %rest:path("/website/manuscripts")
    %output:method("text")
function website:manuscripts() {
    let $syc := local:manuscripts-lang("syc")
    let $ara := local:manuscripts-lang("ara")

    return
        (
            "## The Syriac witnesses",
            $syc,
            "## The Arabic and Karshuni witnesses",
            $ara
        )
};

declare function local:manuscripts-lang($lang as xs:string) {
    let $syriac := $lang eq "syc"
    let $lang := if($syriac) then "syriac" else "arabic-karshuni"
    let $teiHeaders :=
        if($syriac) then
            commons:get-available-aggregates("3r84g")
            ! commons:get-child-tei-uri(.)
            ! doc(.)//tei:teiHeader
        else
            (commons:get-available-aggregates("3r9ps"), commons:get-available-aggregates("3r84h"))
            ! commons:get-child-tei-uri(.)
            ! doc(.)//tei:teiHeader

    for $teiHeader at $pos in $teiHeaders
        where $pos
        let $meta := doc( $commons:meta || ($teiHeader/base-uri() => tokenize("/"))[last()])
        let $tgUri := ($teiHeader/base-uri() => tokenize("/"))[last()] => substring-before(".xml")
        let $msUri := $tgUri => commons:get-parent-uri()
        let $firstpage := string(($teiHeader/root()//tei:pb[@facs][@n])[1]/@n)
        let $url := "https://ahiqar.uni-goettingen.de/" || $lang || "/#/?source=external&amp;redirectUrl=https://ahiqar.uni-goettingen.de/website/&amp;itemurl=https://ahiqar.uni-goettingen.de/textapi/ahiqar/" || $lang || "/" || $msUri || "/" || $firstpage || "/latest/item.json"
    
        let $title := $meta//tgmd:title/text()
        let $title :=
            local:add-empty-line() || "### " || string($title)
    
        let $msIdentifier := local:make-msIdentifier($teiHeader)
    
        let $history := $teiHeader//tei:history
        let $country := $history//tei:country
        let $place-name := $history//tei:placeName
        let $origin-info :=
            if (normalize-space($country) = "" and normalize-space($place-name) = "") then
                "unknown"
            else if (normalize-space($country) != "" and normalize-space($place-name) != "") then
                $country || ", " || $place-name
            else if ($country) then
                $country
            else
                $place-name
                
        let $date-el := $history//tei:date
        let $date :=
            if ($date-el = "unknown" and ($date-el/@notBefore or $date-el/@notAfter)) then
                if ($date-el/@notBefore and $date-el/@notAfter) then
                    "unknown; not before " || $date-el/@notBefore || ", not after " || $date-el/@notAfter
                else if ($date-el/@notBefore) then
                    "unknown; not before " || $date-el/@notBefore
                else
                    "unknown; not after " || $date-el/@notAfter
            else
                $history//tei:date
                
        let $history-info := 
            "* Object type: " || $history//tei:objectType || local:add-newline() ||
            "* Origin: " || $origin-info || local:add-newline() ||
            "* Date of creation: " || $date
        let $history :=
            local:add-empty-line() || "#### History" || local:add-empty-line() || $history-info

        let $listBibl := $teiHeader//tei:listBibl/tei:bibl
        let $bibls :=
                for $bibl in $listBibl
                return
                    if (normalize-space($bibl) != "") then
                        "* " || normalize-space($bibl) || local:add-newline()
                    else
                        ()
        let $listBibl :=
            if (string-join($bibls) => normalize-space() != "") then
                local:add-empty-line() || "#### Bibliography" || local:add-empty-line() || string-join($bibls)
            else
                ()
    
        let $abstract := $teiHeader//tei:abstract
        let $statements := 
            for $p in $abstract//tei:p
                return
                    "* " || normalize-space($p) || local:add-newline()

        let $abstract :=
                local:add-empty-line() || "#### Editor's statements" || local:add-empty-line() || string-join($statements)
    
        return 
            (
                $title,
                local:add-newline() || "view in [edition](" || $url || ")" || local:add-newline(),
                $msIdentifier,
                $history,
                $listBibl,
                $abstract
            )
};

declare function local:make-msIdentifier($teiHeader as element(tei:teiHeader))
as xs:string {
    let $msIdentifier := $teiHeader//tei:msIdentifier
    let $title := local:add-empty-line() || "#### Manuscript Identifier" ||
            local:add-empty-line()
            
    let $links := 
        for $url in $msIdentifier//tei:idno[starts-with(., "http")]
            return "([Digital Source](" || $url || "))"
    let $msIdentifier :=
            "* Identifier: " ||
            $msIdentifier/tei:institution/string() || " (" || $msIdentifier/tei:settlement/string() => normalize-space() || "), " 
                || $msIdentifier/tei:idno/string() || " "
                || $links

    let $further-metadata :=
        for $item in $teiHeader//tei:msItem/* return
            if ($item[self::tei:textLang]) then
                local:add-newline() || "* Language: " || $item/string()
            else if ($item[self::tei:respStmt]) then
                local:add-newline() || "* Responsible: " || $item/tei:persName || " (" || lower-case($item/tei:resp) || ")"
            else
                ()
    let $msItem := string-join($further-metadata, "")
            
    let $result := $title || $msIdentifier || $msItem
            
    return
        $result
};

(:~
 : Provides a list of collation results
:)
declare function local:make-list($lang as xs:string) {
    
    let $sort := function($input) {
                    switch ($input)
                        case "first_narrative_section" return 1
                        case "second_narrative_section" return 2
                        case "third_narrative_section" return 3
                        case "sayings" return 4
                        case "parables" return 5
                        default return 9}
    
    let $resources := xmldb:get-child-resources("/db/data/collation-results/")
    let $stemmas := 
        $resources
        ! substring-before(., "_result")
        ! replace(., "_first_narrative_section|parables|sayings|second_narrative_section|_third_narrative_section", "")
        ! replace(., "_$", "")
        => sort()
        => distinct-values()
    for $i at $pos in $stemmas[starts-with(., $lang)]
    let $iResources := $resources[starts-with(., $i)]
    return
        element xhtml:div {
            element xhtml:h3 { "Stemma " || $pos },
            element xhtml:div {
                "items included: " || replace($i, "ara-karshuni_|syc_", "")
            },
            element xhtml:ol {
                let $units := distinct-values(($iResources ! substring-after(., $i || "_") ! substring-before(., "_result") )) => sort((), $sort)
                for $iii in $units
                return
                    element xhtml:li {
                        ($iii => tokenize("_")) ! (upper-case(substring(., 1, 1)) || substring(., 2, 999)) => string-join(" "), (: ← camel-case function :)
                        element xhtml:ul {
                            for $file in $iResources[contains(., $iii)]
                            order by $file
                            return
                                element xhtml:li {
                                    element xhtml:a {
                                        attribute href { "/rest/collation-results/" || $file },
                                        $file => substring-after(".") => upper-case()
                                    }
                                }
                        }
                    }
            }
        }
};

declare function local:add-empty-line()
as xs:string {
    codepoints-to-string( (10, 10) )
};

declare function local:add-newline()
as xs:string{
    codepoints-to-string( (10) )
};

declare
    %rest:GET
    %rest:HEAD
    %rest:path("/website/collation")
    %output:method("xml")
function website:collation() {
    element xhtml:div {
        element xhtml:h2 {
            text { "Arabic and Karshuni" }
        },
        local:make-list("ara-karshuni"),
        element xhtml:h2 {
            text { "Syriac" }
        },
        local:make-list("syc")
    }
};
xquery version "3.1";

module namespace dbt="http://ahiqar.uni-goettingen.de/ns/database-triggers";

declare namespace trigger="http://exist-db.org/xquery/trigger";

import module namespace commons="http://ahiqar.uni-goettingen.de/ns/commons" at "/db/apps/ahiqar/modules/commons.xqm";
import module namespace san="http://ahiqar.uni-goettingen.de/ns/annotations/save" at "/db/apps/ahiqar/modules/AnnotationAPI/save-annotations.xqm";
import module namespace sh="http://ahiqar.uni-goettingen.de/ns/tapi/html/save" at "/db/apps/ahiqar/modules/save-html.xqm";

declare function trigger:after-create-document($uri as xs:anyURI) {
    dbt:prepare-collections-for-triggers(),
    dbt:process-triggers($uri)
};

declare function trigger:after-update-document($uri as xs:anyURI) {
    dbt:process-triggers($uri)
};

declare function dbt:process-triggers($uri as xs:anyURI) {
    (: use the trigger only in CI to start the transformation :)
    if (not(available-environment-variables() = 'CI_SERVICE')) then () else
    let $teixml-uri := commons:extract-uri-from-base-uri($uri)
    return    
        if (ends-with($uri, ".xml")) then
            let $text-types := commons:get-text-types($teixml-uri)
            let $fragments :=
                <fragments uri="{$teixml-uri}">
                {
                    for $type in $text-types return
                        for $page in commons:get-pages-for-text-type($teixml-uri, $type)
                        return
                            <page n="{$page}" type="{$type}">{commons:get-page-fragment($uri, $page, $type)}</page>
                }
                </fragments>
            return
                (san:make-items-for-TEI($teixml-uri, $fragments),
                sh:save-html($teixml-uri, $fragments))
        else
            ()
};

declare function dbt:prepare-collections-for-triggers() {
    if (xmldb:collection-available($commons:html)) then
        ()
    else
        xmldb:create-collection("/db/data/textgrid", "html"),
    if (xmldb:collection-available($commons:json)) then
        ()
    else
        xmldb:create-collection("/db/data/textgrid", "json")
};

(: to recreate html and annotations, we can call this. mainly for debug. :)
declare function local:recreate-items-for-manifest($uri) {
    let $uri := xs:anyURI("/db/data/textgrid/data/3r678.xml")
    let $urii := commons:get-uri-from-anything($uri)
    let $tei := commons:get-child-tei-uri($urii)
    return
        dbt:process-triggers(xs:anyURI($tei))
};
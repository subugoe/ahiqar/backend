xquery version "3.1";

(: 
 : This module contains the unit tests for tei2html.xqm. 
 :)

module namespace t="http://ahiqar.uni-goettingen.de/ns/tokenize/tests";

declare namespace tei="http://www.tei-c.org/ns/1.0";

import module namespace tokenize="http://ahiqar.uni-goettingen.de/ns/tokenize" at "../modules/tokenize.xqm";
import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";

declare
    %test:assertXPath("$result/local-name() = 'ab' and count($result//text()) = 2 ")
function t:add-ids-single-node()
as node()* {
    let $node := <ab xmlns="http://www.tei-c.org/ns/1.0">some text</ab>
    let $id-prefix := "t_Add_2020"
    return
        tokenize:add-ids($node, $id-prefix)
};

declare
    %test:assertTrue
function t:is-text-relevant-true()
as xs:boolean {
    let $node := <ab xmlns="http://www.tei-c.org/ns/1.0">some text</ab>
    return
        tokenize:is-text-relevant($node/text())
};

declare
    %test:assertFalse
function t:is-text-relevant-false()
as xs:boolean {
    let $node := <ab xmlns="http://www.tei-c.org/ns/1.0"><surplus>some text</surplus></ab>
    return
        tokenize:is-text-relevant($node/descendant::text())
};

declare
    %test:assertXPath("count($result) = 2 
        and $result/local-name() = 'w'
        and tokenize($result[1]/@xml:id, '_')[4] = tokenize($result[2]/@xml:id, '_')[4]
        and tokenize($result[1]/@xml:id, '_')[1] = 't'
        and tokenize($result[1]/@xml:id, '_')[2] = 'Add'")
function t:add-id-to-text()
as element(tei:w)+ {
(:    t_Add_2020_MD152075N1l1_1:)
    let $node := <ab xmlns="http://www.tei-c.org/ns/1.0">some text </ab>
    let $id-prefix := "t_Add_2020"
    return
        tokenize:add-id-to-text($node/text(), $id-prefix)
};

declare
    %test:assertXPath("$result = 't_Add_2020'")
    %test:assertXPath("$result = 't_Sachau_290_Sachau_339'")
    %test:assertXPath("$result = 't_Mingana_ar_christ_93_84'")
function t:make-id-from-idno()
as xs:string+ {
    let $TEIs := (t:create-and-store-test-data(), t:create-and-store-test-data-1(), t:create-and-store-test-data-2())
    for $TEI in $TEIs return
        tokenize:make-id-from-idno($TEI)
};

declare function t:create-and-store-test-data()
as element(tei:TEI) {
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
        <teiHeader>
            <fileDesc>
                <titleStmt>
                    <title>Title</title>
                </titleStmt>
                <publicationStmt>
                    <p>Publication Information</p>
                </publicationStmt>
                <sourceDesc>
                    <msDesc>
                        <msIdentifier>
                            <institution>University of Cambridge - Cambridge University Library</institution>
                            <idno>Add. 2020</idno>
                        </msIdentifier>
                    </msDesc>
                </sourceDesc>
            </fileDesc>
        </teiHeader>
       <text>
            <group>
                <text xml:lang="ara" type="transliteration">
                    <body>
                        <milestone unit="first_narrative_section"/>
                        <ab>Daß alle <sic>unsere</sic> Erkenntnis mit der <surplus>einen</surplus> Erfahrung anfange<supplied>,</supplied> daran </ab>
                                
                        <milestone unit="sayings"/>
                        <ab>Wenn <unclear>aber</unclear> gleich alle unsere Erkenntnis mit der Erfahrung anhebt<g>,</g> so entspringt sie<note>die Erkenntnis</note> darum <w type="colophon">doch nicht eben</w></ab>
                        
                        
                        <milestone unit="second_narrative_section"/>
                        <ab><catchwords>Es ist also wenigstens eine der näheren Untersuchung noch benötigte und nicht auf den</catchwords></ab> 
                    </body>
                </text>
            </group>
        </text>
    </TEI>
};

declare function t:create-and-store-test-data-1()
as element(tei:TEI) {
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
        <teiHeader>
            <fileDesc>
                <sourceDesc>
                    <msDesc>
                        <msIdentifier>
                            <institution>University of Cambridge - Cambridge University Library</institution>
                            <idno>Sachau 290 (=Sachau 339)</idno>
                        </msIdentifier>
                    </msDesc>
                </sourceDesc>
            </fileDesc>
        </teiHeader>
    </TEI>
};

declare function t:create-and-store-test-data-2()
as element(tei:TEI) {
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
        <teiHeader>
            <fileDesc>
                <sourceDesc>
                    <msDesc>
                        <msIdentifier>
                            <institution>University of Cambridge - Cambridge University Library</institution>
                            <idno>Mingana ar. christ. 93[84]</idno>
                        </msIdentifier>
                    </msDesc>
                </sourceDesc>
            </fileDesc>
        </teiHeader>
    </TEI>
};

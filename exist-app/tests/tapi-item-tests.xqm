xquery version "3.1";

module namespace titemt="http://ahiqar.uni-goettingen.de/ns/tapi/item/tests";

declare namespace http = "http://expath.org/ns/http-client";

import module namespace commons="http://ahiqar.uni-goettingen.de/ns/commons" at "../modules/commons.xqm";
import module namespace tc="http://ahiqar.uni-goettingen.de/ns/tests/commons" at "test-commons.xqm";
import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";
import module namespace tapi-item="http://ahiqar.uni-goettingen.de/ns/tapi/item" at "../modules/tapi-item.xqm";


declare
    %test:setUp
function titemt:_test-setup() {
    titemt:create-and-store-test-data()
};


declare
    %test:tearDown
function titemt:_test-teardown() {
    titemt:remove-test-data()
};


declare
    %test:args("sample_edition") %test:assertEquals("Arabic, Classical Syriac, Eastern Syriac, Karshuni, Western Syriac")
function titemt:get-language-string($manifest-uri as xs:string)
as xs:string {
    tapi-item:get-language-string($manifest-uri)
};


declare
    %test:args("sample_edition") 
    %test:assertXPath("array:get($result, 1) => map:get('title') = 'The Proverbs or History of Aḥīḳar the wise, the scribe of Sanḥērībh, king of Assyria and Nineveh'")
function titemt:make-title-object($manifest-uri as xs:string)
as item() {
    tapi-item:make-title-object($manifest-uri)
};


declare
    %test:args("syriac", "sample_edition", "82a")
    (: checks if the correct file has been opened :)
    %test:assertXPath("map:get($result, 'title') => array:get(1) => map:get('title') = 'The Proverbs or History of Aḥīḳar the wise, the scribe of Sanḥērībh, king of Assyria and Nineveh' ")
    %test:assertXPath("map:get($result, 'title') => array:get(1) => map:get('type') = 'main' ")
    (: checks if language assembling works correctly :)
    %test:assertXPath("map:get($result, 'lang') => array:get(2) = 'syc' ")
    %test:assertXPath("map:get($result, 'langAlt') => array:get(1) = 'karshuni' ")
    %test:assertXPath("map:get($result, 'x-langString') => contains('Classical Syriac')")
    (: checks if underlying pages are identified :)
    %test:assertXPath("map:get($result, 'content') => array:get(1) => map:get('url') => contains('/content/transliteration/sample_teixml-82a.html') ")
    %test:assertXPath("map:get($result, 'content') => array:get(1) => map:get('type') = 'application/xhtml+xml;type=transliteration' ")
    (: checks if images connected to underlying pages are identified :)
    %test:assertXPath("map:get($result, 'image') => map:get('id') => contains('/images/restricted/3r1nz/50.03,0.48,49.83,100.00') ")
    %test:assertXPath("map:get($result, 'image') => map:get('license') => map:get('id')")
    %test:assertXPath("map:get($result, 'image') => map:get('license') => map:get('notes')")
    %test:assertXPath("map:get($result, 'annotationCollection') => contains('/annotations/ahiqar/syriac/sample_edition/82a/latest/annotationCollection.json') ")
    (: misc :)
    %test:assertXPath("map:get($result, '@context') = 'https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/item.jsonld'")
    %test:assertXPath("map:get($result, 'id') => contains('textapi/ahiqar/syriac/sample_edition/82a/latest/item.json')")
function titemt:get-json($collection as xs:string,
    $document as xs:string,
    $page as xs:string) 
as map(*) {
    tapi-item:get-json($collection, $document, $page, $tc:server)
};


declare
    %test:args("sample_edition") %test:assertXPath("array:size($result) = 2")
    %test:args("sample_edition") %test:assertExists
function titemt:make-language-array($manifest-uri as xs:string)
as item() {
    tapi-item:make-language-array($manifest-uri)
};

declare
    %test:args("sample_edition") %test:assertXPath("array:size($result) = 3")
    %test:args("sample_edition") %test:assertExists
function titemt:make-langAlt-array($manifest-uri as xs:string)
as item() {
    tapi-item:make-langAlt-array($manifest-uri)
};


declare function titemt:create-and-store-test-data()
as xs:string+ {
    let $agg-wo-tile :=
        <rdf:RDF xmlns:ore="http://www.openarchives.org/ore/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
            <rdf:Description xmlns:tei="http://www.tei-c.org/ns/1.0" rdf:about="textgrid:sample_edition_test.0">
                <ore:aggregates rdf:resource="textgrid:ahiqar_sample_2"/>
            </rdf:Description>
        </rdf:RDF>
    let $agg-wo-tile-meta := commons:get-metadata-file("sample_edition")
        
    let $sample-xml-2 := commons:open-tei-xml("sample_teixml")
    let $sample-xml-2-meta := commons:get-metadata-file("sample_teixml")
        
    return
        (
            xmldb:store("/db/data/textgrid/agg", "ahiqar_agg_wo_tile.xml", $agg-wo-tile),
            xmldb:store("/db/data/textgrid/meta", "ahiqar_agg_wo_tile.xml", $agg-wo-tile-meta),

            xmldb:store("/db/data/textgrid/data", "ahiqar_sample_2.xml", $sample-xml-2),
            xmldb:store("/db/data/textgrid/meta", "ahiqar_sample_2.xml", $sample-xml-2-meta)
        )
};


declare function titemt:remove-test-data() {
    xmldb:remove("/db/data/textgrid/agg", "ahiqar_agg_wo_tile.xml"),
    xmldb:remove("/db/data/textgrid/data", "ahiqar_sample_2.xml"),
    xmldb:remove("/db/data/textgrid/meta", "ahiqar_sample_2.xml"),
    xmldb:remove("/db/data/textgrid/meta", "ahiqar_agg_wo_tile.xml")
};

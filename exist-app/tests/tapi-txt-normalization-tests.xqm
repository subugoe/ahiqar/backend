xquery version "3.1";

module namespace ttnt="http://ahiqar.uni-goettingen.de/ns/tapi/txt/normalization/tests";

import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";
import module namespace norm="http://ahiqar.uni-goettingen.de/ns/tapi/txt/normalization" at "../modules/tapi-txt-normalization.xqm";


declare
    %test:setUp
function ttnt:_test-setup() {
    xmldb:create-collection("/db", "tmp"),
    ttnt:create-and-store-test-data()
};


declare
    %test:tearDown
function ttnt:_test-teardown() {
    xmldb:remove("/db/tmp")
};


declare
    %test:args("ܬܟܼܫܪ") %test:assertXPath("not($result = 1852)")
    %test:args("ܬܟܫܪ") %test:assertXPath("$result = 1823")
    %test:args("ܕܩܿܐܡ") %test:assertXPath("not($result = 1855)")
function ttnt:remove-codepoints($string as xs:string)
as xs:integer+ {
    let $codepoints := string-to-codepoints($string)
    return
        norm:remove-codepoints($codepoints)
};



declare
    %test:assertEquals("Some text")
function ttnt:get-txt-without-diacritics() {
    util:binary-doc("/db/tmp/test.txt")
    => util:base64-decode()
    => norm:get-txt-without-diacritics()
};

declare
    %test:assertEquals("ܒܡܬܠܐ")
function ttnt:get-txt-without-diacritics-2() {
    norm:get-txt-without-diacritics("ܒܡ̈ܬܠܐ")
};

declare
    %test:assertEquals("ܒܡܬܠܐ")
function ttnt:get-txt-without-diacritics-3() {
    norm:get-txt-without-diacritics("ܒܡܬ̈ܠܐ")
};

declare
    %test:assertEquals("ܡܢ")
function ttnt:get-txt-without-diacritics-4() {
    norm:get-txt-without-diacritics("ܡܿܢ")
};

declare
    %test:assertEquals("ܗܘ")
function ttnt:get-txt-without-diacritics-5() {
    norm:get-txt-without-diacritics("ܗ̄ܘܼ")
};

declare
    %test:assertEquals("ܗܘ")
function ttnt:get-txt-without-diacritics-6() {
    norm:get-txt-without-diacritics("ܗ̄ܘ")
};

declare function ttnt:create-and-store-test-data() {
    let $txt := "Some text."
    let $txt-cps := string-to-codepoints($txt)
    
    let $mix :=
        for $iii in 1 to 10 return
            insert-before(($norm:syriac-vowels), $iii * 3, $txt-cps[$iii])
    let $mix-str := codepoints-to-string($mix)
    return
        xmldb:store-as-binary("/db/tmp", "test.txt", $mix-str)
};

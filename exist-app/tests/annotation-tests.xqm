xquery version "3.1";

module namespace at="http://ahiqar.uni-goettingen.de/ns/annotations/tests";

declare namespace http = "http://expath.org/ns/http-client";
declare namespace tei="http://www.tei-c.org/ns/1.0";

import module namespace commons="http://ahiqar.uni-goettingen.de/ns/commons" at "../modules/commons.xqm";
import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";
import module namespace anno="http://ahiqar.uni-goettingen.de/ns/annotations" at "../modules/AnnotationAPI/annotations.xqm";

declare variable $at:sample-doc := doc($commons:data || "/sample_teixml.xml");

declare
    %test:args("sample_teixml") %test:assertTrue
    %test:args("sample_edition") %test:assertFalse
function at:is-resource-xml($uri as xs:string)
as xs:boolean {
    anno:is-resource-xml($uri)
};

declare
    %test:assertTrue
function at:are-resources-available-true()
as xs:boolean {
    let $resources := "sample_teixml"
    return
        anno:are-resources-available($resources)
};

declare
    %test:assertTrue
function at:are-resources-available-lang-collection-true()
as xs:boolean {
    let $resources := "syriac"
    return
        anno:are-resources-available($resources)
};

declare
    %test:assertFalse
function at:are-resources-available-false()
as xs:boolean {
    let $resources := ("qwerty", "sample_teixml")
    return
        anno:are-resources-available($resources)
};

declare
    %test:args("sample_teixml", "82a", "next") %test:assertEquals("82b")
    %test:args("sample_teixml", "82b", "prev") %test:assertEquals("82a")
    %test:args("sample_teixml", "83b", "next") %test:assertEmpty
    %test:args("sample_teixml", "82a", "prev") %test:assertEmpty
    %test:args("sample_edition", "82a", "prev") %test:assertEmpty
    %test:args("sample_edition", "82b", "prev") %test:assertEquals("82a")
    %test:pending
function at:get-prev-or-next-page($documentURI as xs:string,
    $page as xs:string, 
    $type as xs:string)
as xs:string? {
    anno:get-prev-or-next-page($documentURI, $page, $type)
};

declare
    %test:args("sample_teixml") %test:assertEquals("Beispieldatei zum Testen")
function at:get-metadata-title($uri as xs:string)
as xs:string {
    anno:get-metadata-title($uri)
};

declare
    %test:args("syriac", "sample_edition", "82a", "http://localhost:8080")
    %test:assertXPath("map:get($result, 'id') = 'http://ahiqar.uni-goettingen.de/ns/annotations/annotationCollection/sample_edition/82a'")
    %test:assertXPath("map:get($result, 'type') = 'AnnotationCollection'")
    %test:assertXPath("map:get($result, 'label') = 'Ahiqar annotations for textgrid:sample_edition: Beispieldatei zum Testen, page 82a'")
function at:make-annotationCollection-for-manifest($collection as xs:string,
    $document as xs:string,
    $page as xs:string,
    $server as xs:string)
as map() {
    anno:make-annotationCollection-for-manifest($collection, $document, $page, $server)
};


declare
    %test:assertEquals("sample_teixml")
function at:get-all-xml-uris-for-submap()
as xs:string* {
    let $map :=
         map {
            "sample_lang_aggregation_syriac":
            map {
                "sample_edition": "sample_teixml",
                "faux_edition": "faux_teixml"
            }
         }
    return
        anno:get-all-xml-uris-for-submap($map)
};


declare
    %test:args("sample_edition") %test:assertEquals("sample_teixml")
function at:find-in-map($key as xs:string)
as item()? {
    let $map :=
        map {
                "sample_lang_aggregation_syriac":
                map {
                    "sample_edition": "sample_teixml",
                    "faux_edition": "faux_teixml"
                }
             }  
    
    return
        anno:find-in-map($map, $key)
};


declare
    %test:args("sample_edition", "11")
    %test:assertXPath("ends-with($result, 'annotations/ahiqar/sample_lang_aggregation_syriac/sample_edition/11/latest/annotationPage.json')")
    %test:args("sample_edition", "")
    %test:assertXPath("ends-with($result, 'annotations/ahiqar/sample_lang_aggregation_syriac/sample_edition/latest/annotationPage.json')")
    %test:args("", "")
    %test:assertEmpty
function at:get-prev-or-next-annotationPage-url($document as xs:string?,
    $page as xs:string?)
as xs:string? {
    let $collection := "sample_lang_aggregation_syriac"
    let $server := $commons:server
    return
        anno:get-prev-or-next-annotationPage-url($collection, $document, $page, $server)
};

declare
    %test:args("sample_edition") %test:assertTrue
    %test:args("sample_lang_aggregation_syriac") %test:assertFalse
function at:is-resource-edition($uri as xs:string) {
    let $map := 
        map {
                "sample_lang_aggregation_syriac":
                map {
                    "sample_edition": "sample_teixml",
                    "faux_edition": "faux_teixml"
                }
             }  
    return
        anno:is-resource-edition($map, $uri)
};

declare
    %test:args("sample_teixml") %test:assertEquals("Simon Birol, Aly Elrefaei")
    %test:args("sample_edition") %test:assertEquals("Simon Birol, Aly Elrefaei")
    %test:args("syriac") %test:assertEquals("Simon Birol, Aly Elrefaei")
    %test:args("arabic-karshuni") %test:assertEquals("Simon Birol, Aly Elrefaei")
function at:get-creator($uri as xs:string)
as xs:string {
    anno:get-creator($uri)
};

declare
    %test:args("sample_edition", 
        "Beispieledition", 
        "/annotations/ahiqar/sample_lang_aggregation_syriac/sample_edition/82a/latest/annotationPage.json", 
        "/annotations/ahiqar/sample_lang_aggregation_syriac/sample_edition/83b/latest/annotationPage.json")
    %test:assertXPath("map:get($result, 'label') = 'Ahiqar annotations for textgrid:sample_edition: Beispieledition'")
    %test:assertXPath("map:get($result, 'x-creator') = 'Simon Birol, Aly Elrefaei'")
function at:make-annotationCollection-map($uri as xs:string,
    $title as xs:string,
    $first-entry as xs:string,
    $last-entry as xs:string)
as map() {

    anno:make-annotationCollection-map($uri, $title, $commons:server || $first-entry, $commons:server || $last-entry)
};

declare
    %test:args("sample_lang_aggregation_syriac", "sample_edition", "http://localhost:8080")
    %test:assertXPath("map:get('id') = 'http://ahiqar.uni-goettingen.de/ns/annotations/annotationPage/sample_lang_aggregation_syriac/sample_edition'")
    %test:pending
function at:make-annotationPage($collection as xs:string, 
    $manifest as xs:string,
    $server as xs:string)
as map() {
    anno:make-annotationPage($collection, $manifest, $server)
};

declare
    %test:args("next", "1r") %test:assertEquals("1v")
    %test:args("prev", "1r") %test:assertEmpty
    %test:args("next", "2r") %test:assertEmpty
    %test:args("prev", "2r") %test:assertEquals("1ab")
function at:get-prev-or-next($type as xs:string,
    $searched-for as xs:string)
as xs:string? {
    let $entities := ("1r", "1v", "1ab", "2r")
    return
        anno:get-prev-or-next($entities, $searched-for, $type)
};

declare
    %test:args("syriac", "sample_edition", "next") %test:assertTrue
    %test:args("syriac", "sample_edition", "prev") %test:assertTrue
function at:get-prev-or-next-annotationPage-ID($collection as xs:string,
    $document as xs:string,
    $type as xs:string)
as xs:boolean {
    let $result := anno:get-prev-or-next-annotationPage-ID($collection, $document, $type)
    return
        $result = "sample_edition_syriac" or empty($result)
};

declare
    %test:args("sample_lang_aggregation_syriac", "sample_edition", "84a", "http://localhost:8080")
    %test:assertEquals("http://ahiqar.uni-goettingen.de/ns/annotations/annotationPage/sample_lang_aggregation_syriac/sample_edition-84a")
function at:make-annotationPage-for-manifest-id($collection as xs:string,
    $document as xs:string,
    $page as xs:string,
    $server as xs:string)
as xs:string {
    let $result := anno:make-annotationPage-for-manifest($collection, $document, $page, $server)
    return
        map:get($result, "id")
};


declare
    %test:args("sample_lang_aggregation_syriac", "sample_edition", "84a", "http://localhost:8080")
    %test:assertEquals("http://ahiqar.uni-goettingen.de/ns/annotations/annotationCollection/sample_edition")
function at:make-annotationPage-for-manifest-part-of($collection as xs:string,
    $document as xs:string,
    $page as xs:string,
    $server as xs:string)
as xs:string {
    let $result := anno:make-annotationPage-for-manifest($collection, $document, $page, $server)
    return
        map:get($result, "partOf")
        => map:get("id")
};

declare
    %test:args("syriac", "http://localhost:8080")
    %test:assertTrue
function at:get-information-for-collection-object($collection-type as xs:string,
    $server as xs:string)
as xs:boolean {
    let $result := anno:get-information-for-collection-object($collection-type, $server)
    let $first :=
        map:get($result, "first")
    return
        matches($first, "http://.+/annotations/ahiqar/syriac/sample_edition(_syriac)?/(\d+[rv])/latest/annotationPage.json")
};

declare
    %test:args("syriac", "sample_edition", "http://localhost:8080")
    %test:assertXPath("$result instance of map()")
    %test:args("syriac", "", "http://localhost:8080")
    %test:assertXPath("$result instance of map()")
function at:make-annotationCollection($collection as xs:string,
    $document as xs:string?,
    $server as xs:string)
as map() {
    anno:make-annotationCollection($collection, $document, $server)
};

declare
    %test:args("syriac") %test:assertEquals("sample_lang_aggregation_syriac")
    %test:args("arabic-karshuni") %test:assertXPath("count($result) = 2 and $result = 'sample_lang_aggregation_arabic' and $result = 'sample_lang_aggregation_karshuni'")
function at:get-lang-aggregation-uris($collection-type as xs:string)
as xs:string+ {
    anno:get-lang-aggregation-uris($collection-type)
};

declare
    %test:args("syriac") %test:assertEquals("The Syriac Collection")
    %test:args("arabic-karshuni") %test:assertEquals("The Arabic and Karshuni Collections")
    %test:args("sample_edition") %test:assertEquals("Beispieldatei zum Testen")
    %test:args("asdf") %test:assertError("COMMONS002")
function at:make-collection-object-title($collection-type as xs:string) {
    anno:make-collection-object-title($collection-type)
};

declare
    %test:args("syriac") %test:assertEquals("sample_lang_aggregation_syriac")
    %test:args("arabic-karshuni") %test:assertXPath("count($result) = 2 and $result = 'sample_lang_aggregation_arabic' and $result = 'sample_lang_aggregation_karshuni'")
    %test:args("sample_edition") %test:assertEquals("sample_edition")
function at:determine-uris-for-collection($collection as xs:string)
as xs:string+ {
    anno:determine-uris-for-collection($collection)
};

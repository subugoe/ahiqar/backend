xquery version "3.1";

module namespace t="http://ahiqar.uni-goettingen.de/ns/annotations/variants/tests";

import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";
import module namespace vars="http://ahiqar.uni-goettingen.de/ns/annotations/variants" at "../modules/AnnotationAPI/variants.xqm";

declare variable $t:sample-map :=
    map {
        "variants": (map {
            "entry": "ܐܘ",
            "witness": "430"
        },map {
            "entry": "ܕܐܚܛܛ",
            "witness": "syr_422"
        },map {
            "entry": [],
            "witness": "syr_611"
        },map {
            "entry": [],
            "witness": "syr_612"
        }),
        "current": map {
            "t": "ܐܡܪ",
            "id": "syr_434_N4.4.2.6.4.8.1.3_1"
        }
    };
    
declare variable $t:sample-file :=
    util:binary-doc("/db/apps/ahiqar/data/collation-results/ara-karshuni_Sbath_25_Vat_sir_424_Vat_sir_199_parables_result.json")
    => util:base64-decode()
    => parse-json();

declare
    %test:args("sample_teixml") %test:assertXPath("count($result) = 175")
function t:get-token-ids-on-page($teixml-uri as xs:string)
as xs:string+ {
    let $page :=
        <TEI xmlns="http://www.tei-c.org/ns/1.0">
            <text>
                <group>
                    <text xml:lang="ara">
                        <body xml:lang="ara">
                            <pb id="N1.4.2.2.4.2" n="82a"/>
                            <cb id="N1.4.2.2.4.4"/>
                            <head id="N1.4.2.2.4.6">
                                <supplied id="N1.4.2.2.4.6.2">.</supplied>
                                <add id="N1.4.2.2.4.6.4" place="margin">
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.7.5.3_1" type="token">حقًا</w>
                                </add>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.7.6_1" type="token">حيث</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.7.6_2" type="token">تبدأ</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.7.6_3" type="token">القصة</w>
                                <g id="N1.4.2.2.4.6.6">✓</g>
                            </head>
                            <ab id="N1.4.2.2.4.8">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.9.2_1" type="token">الحاسوب</w>
                            </ab>
                            <ab id="N1.4.2.2.4.10">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.11.2_1" type="token">نبتدي</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.11.2_2" type="token">بعون</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.11.2_3" type="token">الباري</w>
                                <add id="N1.4.2.2.4.10.2" place="interlinear">
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.11.3.3_1" type="token">اختبار</w>
                                </add>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.11.4_1" type="token">تعالى</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.11.4_2" type="token">جل</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.11.4_3" type="token">اسمه</w>
                            </ab>
                            <ab id="N1.4.2.2.4.12">
                                <lb id="N1.4.2.2.4.12.2" break="no"/>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.13.4_1" type="token">وتعالى</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.13.4_2" type="token">ذكره</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.13.4_3" type="token">الى</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.13.4_4" type="token">الابد.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.13.4_5" type="token">ونكتب</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.13.4_6" type="token">خبر</w>
                            </ab>
                            <ab id="N1.4.2.2.4.14">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.15.2_1" type="token">الحكيم</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.15.2_2" type="token">الماهر</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.15.2_3" type="token">الفليوس</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.15.2_4" type="token">الشاطر</w>
                            </ab>
                            <ab id="N1.4.2.2.4.16">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.17.2_1" type="token">وزير</w>
                                <persName id="N1.4.2.2.4.16.2">
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.17.3.2_1" type="token">سنحاريب</w>
                                </persName>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.17.4_1" type="token">ابن</w>
                                <persName id="N1.4.2.2.4.16.4">
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.17.5.2_1" type="token">سرحادوم</w>
                                </persName>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.17.6_1" type="token">ملك</w>
                                <unclear id="N1.4.2.2.4.16.6" reason="illegible">
                                    <placeName id="N1.4.2.2.4.16.6.3">اتور</placeName>
                                </unclear>
                            </ab>
                            <milestone id="N1.4.2.2.4.18" unit="first_narrative_section"/>
                            <ab id="N1.4.2.2.4.20">
                                <placeName id="N1.4.2.2.4.20.2">
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.21.3.2_1" type="token">ونينوى</w>
                                </placeName>
                                <placeName id="N1.4.2.2.4.20.4">
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.21.5.2_1" type="token">والموصل</w>
                                </placeName>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.21.6_1" type="token">.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.21.6_2" type="token">وما</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.21.6_3" type="token">جرا</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.21.6_4" type="token">منه</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.21.6_5" type="token">ومن</w>
                            </ab>
                            <ab id="N1.4.2.2.4.22">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.23.2_1" type="token">ابن</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.23.2_2" type="token">اخته</w>
                                <persName id="N1.4.2.2.4.22.2">
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.23.3.2_1" type="token">نادان</w>
                                </persName>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.23.4_1" type="token">.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.23.4_2" type="token">كان</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.23.4_3" type="token">في</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.23.4_4" type="token">ايام</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.23.4_5" type="token">الملك</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.23.4_6" type="token">ابن</w>
                                <persName id="N1.4.2.2.4.22.4">
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.23.5.2_1" type="token">سرحادوم</w>
                                </persName>
                            </ab>
                            <ab id="N1.4.2.2.4.24">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.25.2_1" type="token">ملك</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.25.2_2" type="token">ارض</w>
                                <placeName id="N1.4.2.2.4.24.2">
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.25.3.2_1" type="token">اتور</w>
                                </placeName>
                                <placeName id="N1.4.2.2.4.24.4">
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.25.5.2_1" type="token">ونينوى</w>
                                </placeName>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.25.6_1" type="token">وبلادها.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.25.6_2" type="token">رجل</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.25.6_3" type="token">حكيم</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.25.6_4" type="token">يقال</w>
                            </ab>
                            <ab id="N1.4.2.2.4.26">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.27.2_1" type="token">له</w>
                                <persName id="N1.4.2.2.4.26.2">
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.27.3.2_1" type="token">حيقار</w>
                                </persName>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.27.4_1" type="token">.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.27.4_2" type="token">وكان</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.27.4_3" type="token">وزير</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.27.4_4" type="token">الملك</w>
                                <persName id="N1.4.2.2.4.26.4">
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.27.5.2_1" type="token">سنحاريب</w>
                                </persName>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.27.6_1" type="token">وكاتبه</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.27.6_2" type="token">وكان</w>
                            </ab>
                            <ab id="N1.4.2.2.4.28">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.29.2_1" type="token">ذو</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.29.2_2" type="token">مال</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.29.2_3" type="token">جزيل</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.29.2_4" type="token">ورزق</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.29.2_5" type="token">كثير.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.29.2_6" type="token">وكان</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.29.2_7" type="token">ماهر</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.29.2_8" type="token">حكيم</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.29.2_9" type="token">فيلسوف</w>
                            </ab>
                            <ab id="N1.4.2.2.4.30">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.31.2_1" type="token">ذو</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.31.2_2" type="token">معرفه</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.31.2_3" type="token">وراي</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.31.2_4" type="token">وتدبير.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.31.2_5" type="token">وكان</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.31.2_6" type="token">قد</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.31.2_7" type="token">تزوج</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.31.2_8" type="token">ستين</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.31.2_9" type="token">امراه</w>
                            </ab>
                            <milestone id="N1.4.2.2.4.32" unit="second_narrative_section"/>
                            <cb id="N1.4.2.2.4.34"/>
                            <ab id="N1.4.2.2.4.36" type="head">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.37.3_1" type="token">رأس</w>
                                <add id="N1.4.2.2.4.36.3" place="header">
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.37.4.3_1" type="token">اختبار</w>
                                </add>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.37.5_1" type="token">متقاطع</w>
                            </ab>
                            <ab id="N1.4.2.2.4.38">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.39.2_1" type="token">وبنى</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.39.2_2" type="token">لكل</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.39.2_3" type="token">واحده</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.39.2_4" type="token">منهن</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.39.2_5" type="token">مقصوره.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.39.2_6" type="token">ومع</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.39.2_7" type="token">هذا</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.39.2_8" type="token">كله</w>
                            </ab>
                            <ab id="N1.4.2.2.4.40">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.41.2_1" type="token">لم</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.41.2_2" type="token">يكن</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.41.2_3" type="token">له</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.41.2_4" type="token">ولد</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.41.2_5" type="token">يرثه.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.41.2_6" type="token">وكان</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.41.2_7" type="token">كثير</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.41.2_8" type="token">الهم</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.41.2_9" type="token">لاجل</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.41.2_10" type="token">ذلك</w>
                            </ab>
                            <ab id="N1.4.2.2.4.42">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.43.2_1" type="token">وانه</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.43.2_2" type="token">في</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.43.2_3" type="token">ذات</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.43.2_4" type="token">يوم</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.43.2_5" type="token">جمع</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.43.2_6" type="token">المنجمين</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.43.2_7" type="token">والسحره</w>
                            </ab>
                            <ab id="N1.4.2.2.4.44">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.45.2_1" type="token">والعارفين.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.45.2_2" type="token">واشكا</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.45.2_3" type="token">لهم</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.45.2_4" type="token">حاله</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.45.2_5" type="token">وامر</w>
                                <choice id="N1.4.2.2.4.44.2">
                                    <sic id="N1.4.2.2.4.44.2.2">بعقوريته</sic>
                                    <corr id="N1.4.2.2.4.44.2.4">
                                        <w xml:id="t_Add_2020_N1.5.3.3.5.45.3.5.2_1" type="token"
                                            >عقوريته</w>
                                    </corr>
                                </choice>
                                <choice id="N1.4.2.2.4.44.4">
                                    <sic id="N1.4.2.2.4.44.4.2">بعقوريته</sic>
                                    <corr id="N1.4.2.2.4.44.4.4" resp="#sb">
                                        <w xml:id="t_Add_2020_N1.5.3.3.5.45.5.5.3_1" type="token"
                                            >عقوريته</w>
                                    </corr>
                                </choice>
                            </ab>
                            <ab id="N1.4.2.2.4.46">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.47.2_1" type="token">فقالوا</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.47.2_2" type="token">له</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.47.2_3" type="token">ادخل</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.47.2_4" type="token">ادبح</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.47.2_5" type="token">للالهه</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.47.2_6" type="token">واستجير</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.47.2_7" type="token">بهم</w>
                            </ab>
                            <ab id="N1.4.2.2.4.48">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.49.2_1" type="token">لعلهم</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.49.2_2" type="token">يرزقوك</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.49.2_3" type="token">ولدًا.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.49.2_4" type="token">ففعل</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.49.2_5" type="token">كما</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.49.2_6" type="token">قالوا</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.49.2_7" type="token">له.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.49.2_8" type="token">وقدم</w>
                            </ab>
                            <ab id="N1.4.2.2.4.50">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.51.2_1" type="token">القرابين</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.51.2_2" type="token">للاصنام</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.51.2_3" type="token">واستغاث</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.51.2_4" type="token">بهم.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.51.2_5" type="token">وتضرّع</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.51.2_6" type="token">اليهم</w>
                            </ab>
                            <ab id="N1.4.2.2.4.52">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.53.2_1" type="token">بالطلبه</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.53.2_2" type="token">والدعا.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.53.2_3" type="token">فلم</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.53.2_4" type="token">يجيبوه</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.53.2_5" type="token">بكلمه.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.53.2_6" type="token">فخرج</w>
                            </ab>
                            <ab id="N1.4.2.2.4.54">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.55.2_1" type="token">يحزان</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.55.2_2" type="token">ندمان</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.55.2_3" type="token">خايب</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.55.2_4" type="token">وانصرف</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.55.2_5" type="token">متالم</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.55.2_6" type="token">القلب.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.55.2_7" type="token">ورجع</w>
                            </ab>
                            <ab id="N1.4.2.2.4.56">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.57.2_1" type="token">بالتضرع</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.57.2_2" type="token">الى</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.57.2_3" type="token">الله</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.57.2_4" type="token">تعالى.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.57.2_5" type="token">وامن</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.57.2_6" type="token">واستعان</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.57.2_7" type="token">به</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.57.2_8" type="token">بحرقة</w>
                            </ab>
                            <ab id="N1.4.2.2.4.58">
                                <w xml:id="t_Add_2020_N1.5.3.3.5.59.2_1" type="token">قلب</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.59.2_2" type="token">قايلًا.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.59.2_3" type="token">يا</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.59.2_4" type="token">الاه</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.59.2_5" type="token">السما</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.59.2_6" type="token">والارض.</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.59.2_7" type="token">يا</w>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.59.2_8" type="token">خالق</w>
                            </ab>
                            <lg id="N1.4.2.2.4.60">
                                <l id="N1.4.2.2.4.60.2">
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.61.3.2_1" type="token">هل</w>
                                    <add id="N1.4.2.2.4.60.2.2" place="below">
                                        <w xml:id="t_Add_2020_N1.5.3.3.5.61.3.3.3_1" type="token">اختبار</w>
                                    </add>
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.61.3.4_1" type="token">أقارنك</w>
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.61.3.4_2" type="token">بيوم</w>
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.61.3.4_3" type="token">صيفي؟</w>
                                </l>
                                <l id="N1.4.2.2.4.60.4">
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.61.5.2_1" type="token">انت</w>
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.61.5.2_2" type="token">أكثر</w>
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.61.5.2_3" type="token">جميلة</w>
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.61.5.2_4" type="token">وأكثر</w>
                                    <w xml:id="t_Add_2020_N1.5.3.3.5.61.5.2_5" type="token">اعتدالا</w>
                                    <surplus id="N1.4.2.2.4.60.4.2">:</surplus>
                                </l>
                            </lg>
                            <ab id="N1.4.2.2.4.62">
                                <seg id="N1.4.2.2.4.62.2" type="colophon">. نهاية<add id="N1.4.2.2.4.62.2.3"
                                        place="footer">اختبار</add> النص</seg>
                                <w xml:id="t_Add_2020_N1.5.3.3.5.63.4_1" type="token">وماتت.</w>
                            </ab>
                            <pb id="N1.4.2.2.4.64" n="82b"/>
                        </body>
                    </text>
                </group>
            </text>
        </TEI>
    return
        vars:get-token-ids-on-page($teixml-uri, $page)
};


declare
    %test:args("sample_teixml") %test:assertEquals("Add_2020")
function t:get-ms-id-from-idno($teixml-uri as xs:string)
as xs:string {
    vars:get-ms-id-from-idno($teixml-uri)
};

declare 
    %test:args("Sachau_336") %test:assertXPath("count($result) = 5")
function t:get-relevant-files($ms-id as xs:string)
as item()+ {
    vars:get-relevant-files($ms-id)
};

declare
    %test:args("Ar_7/229") %test:assertEquals("4")
function t:determine-id-position($ms-id as xs:string)
as xs:integer {
    let $json := vars:get-relevant-files($ms-id)
    return
        vars:determine-id-position($ms-id, $json[1])
};

declare
    %test:assertEquals("Variant")
function t:get-body-object() {
    vars:get-body-object($t:sample-map)
    => map:get("x-content-type")
};

declare
    %test:assertXPath("count($result) = 2")
function t:get-files-relevant-for-page() {
    let $relevant-files-for-ms-id := vars:get-relevant-files("Sbath_25")
    let $ms-id-position := 1
    let $tokens := ("Sbath_25_N4.4.2.4.4.2552.1_1", "Sbath_25_N4.4.2.4.4.500.1_1")
    return
        vars:get-files-relevant-for-page($relevant-files-for-ms-id, $ms-id-position, $tokens)
};

declare
    %test:assertEquals("1")
function t:get-indices-relevant-for-page() {
    let $table :=
        $t:sample-file
        => map:get("table")
    let $no-of-sequences := array:size($table)
    let $ms-id-position := 1
    let $tokens := ("Sbath_25_N4.4.2.4.4.2276.1_1", "Sbath_25_N4.4.2.4.4.2276.2.1_1", "Sbath_25_N4.4.2.4.4.2276.3_1", "Sbath_25_N4.4.2.4.4.2276.3_2", "Sbath_25_N4.4.2.4.4.2276.3_3")
    return
        vars:get-indices-relevant-for-page($table, $no-of-sequences, $ms-id-position, $tokens)
};

declare
    %test:assertXPath("count($result) = 2 and $result = (2, 3)")
function t:get-non-ms-id-positions-in-array() {
    vars:get-non-ms-id-positions-in-array($t:sample-file, 1)
};

declare
    %test:assertXPath("$result = ('ܕܐܚܛܛ', 'N')")
function t:get-target-information() {
    vars:make-annotation-value($t:sample-map)[2]
    => map:get("entry"),
    vars:make-annotation-value($t:sample-map)[2]
    => map:get("witness")
};

declare
    %test:assertEquals("ara")
function t:get-target-language() {
    vars:get-target-language("kant_sample_teixml")
};

declare
    %test:assertEquals("Sbath_25")
function t:get-witness() {
    vars:get-witness($t:sample-file, 1)
};

declare
    %test:assertEquals("D")
function t:make-annotation-value() {
    vars:make-annotation-value($t:sample-map)[1]
    => map:get("witness")
};

declare
    %test:assertXPath("$result = ('Sbath_25_N4.4.2.4.4.2276.3_3', 'Vat_sir_424', 'Vat_sir_199')")
function t:make-map-for-token() {
    let $table := map:get($t:sample-file, "table")
    let $entry-pos := 1
    let $ms-id-position := 1
    let $non-ms-id-positions := (2, 3)
    let $current := 
        vars:make-map-for-token($t:sample-file, $table, $entry-pos, $ms-id-position, $non-ms-id-positions)
        => map:get("current")
        => map:get("id")
    let $variants := 
        vars:make-map-for-token($t:sample-file, $table, $entry-pos, $ms-id-position, $non-ms-id-positions)
        => map:get("variants")
    return
        (
            $current,
            $variants[1] => map:get("witness"),
            $variants[2] => map:get("witness")
            
        )
        
};
xquery version "3.1";

module namespace t="http://ahiqar.uni-goettingen.de/ns/annotations/editorial/tests";

declare namespace tei="http://www.tei-c.org/ns/1.0";

import module namespace commons="http://ahiqar.uni-goettingen.de/ns/commons" at "../modules/commons.xqm";
import module namespace edit="http://ahiqar.uni-goettingen.de/ns/annotations/editorial" at "../modules/AnnotationAPI/editorial.xqm";
import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";

declare variable $t:sample-doc := doc($commons:data || "/sample_teixml.xml");

declare
    %test:assertEquals("ܐܬܘܪ")
function t:anno-get-body-value()
as xs:string {
    let $annotation := $t:sample-doc//tei:text[@type = "transcription"]/descendant::tei:placeName[1]
    return
        edit:get-body-value($annotation)
};

declare
    %test:assertEquals("correction of faulty text. original: errror, corrected by the scribe to: error")
function t:anno-get-body-value-scribe()
as xs:string {
    let $annotation :=
        <choice xmlns="http://www.tei-c.org/ns/1.0">
            <sic>errror</sic>
            <corr>error</corr>
        </choice>
    return
        edit:get-body-value($annotation)
};

declare
    %test:assertEquals("correction of faulty text. original: errror, corrected by the editor to: error")
function t:anno-get-body-value-editor()
as xs:string {
    let $annotation :=
        <choice xmlns="http://www.tei-c.org/ns/1.0">
            <sic>errror</sic>
            <corr resp="#sb">error</corr>
        </choice>
    return
        edit:get-body-value($annotation)
};


declare
    %test:assertEquals("Person")
function t:get-annotation-type-person()
as xs:string {
    let $annotation := $t:sample-doc//tei:text[@type = "transcription"]/descendant::tei:persName[1]
    return
        edit:get-annotation-type($annotation)
};

declare
    %test:assertEquals("Place")
function t:get-annotation-type-place()
as xs:string {
    let $annotation := $t:sample-doc//tei:text[@type = "transcription"]/descendant::tei:placeName[1]
    return
        edit:get-annotation-type($annotation)
};

declare
    %test:args("sample_teixml", "N1l4l2l4l4l763l4")
    %test:assertXPath("map:get($result, 'selector') => map:get('value') = '#N1l4l2l4l4l763l4'")
    %test:assertXPath("map:get($result, 'selector') => map:get('type') = 'CssSelector'")
    %test:assertXPath("map:get($result, 'format') = 'text/xml'")
    %test:assertXPath("map:get($result, 'language') = 'syc'")
function t:get-target-information($documentURI as xs:string,
    $id as xs:string)
as map() {
    let $page := <TEI xmlns="http://www.tei-c.org/ns/1.0">
            <text>
                <group>
                    <text xml:lang="syc">
                        <body xml:lang="syc">
                            <pb id="N1l4l2l4l4l759" n="10b" facs="textgrid:3vwb8"/>
                            <ab id="N1l4l2l4l4l763">
                                <persName id="N1l4l2l4l4l763l4">
                                    <w xml:id="t_433_N1l5l3l5l5l764l5l3_w_1" type="token">ܕܣܲܪ</w>
                                </persName>
                            </ab>
                            <pb id="N1l4l2l4l4l801" n="11a" facs="textgrid:3vwb9"/>
                        </body>
                    </text>
                </group>
            </text>
        </TEI>
    let $annotation := $page//tei:persName
    return
        edit:get-target-information($annotation, $documentURI, $id)
};

declare
    %test:args("sample_teixml", "83b") %test:assertXPath("count($result) ge 60")
    %test:args("sample_syriac_teixml", "86r") %test:assertXPath("count($result) ge 7")
function t:get-annotations($teixml-uri as xs:string,
    $page as xs:string)
as map()+ {
    let $tei-xml-base-uri := commons:get-document($teixml-uri, "data")/base-uri()
    let $pages := commons:get-page-fragment($tei-xml-base-uri, $page, "transcription")
    return
        edit:get-annotations($pages, $teixml-uri)
};

declare
    %test:args("sample_teixml", "84a")
    %test:assertXPath("map:get($result, 'value') = 'ܢܕܢ܂'")
function t:get-annotations-detailed-body($teixml-uri as xs:string,
    $page as xs:string)
as map() {
    let $pages := commons:get-page-fragments($teixml-uri, $page)
    let $result-map := edit:get-annotations($pages, $teixml-uri)[1]
    let $bodyValue := map:get($result-map, "body")
    return
        $bodyValue
};

declare
    %test:args("sample_teixml", "84a")
    %test:assertXPath("matches($result, '^http://ahiqar.uni-goettingen.de/ns/annotations/sample_teixml/annotation-[0-9A-Z]+')")
function t:get-annotations-detailed-id($teixml-uri as xs:string,
    $page as xs:string)
as xs:string {
    let $pages := commons:get-page-fragments($teixml-uri, $page)
    let $result-map := edit:get-annotations($pages, $teixml-uri)[1]
    let $id := map:get($result-map, "id")
    return
        $id
};

declare
    %test:args("sample_teixml", "84a")
    %test:assertXPath("$result instance of array(map())")
function t:get-annotations-detailed-target($teixml-uri as xs:string,
    $page as xs:string)
as array(map()) {
    let $pages := commons:get-page-fragments($teixml-uri, $page)
    let $result-map := edit:get-annotations($pages, $teixml-uri)[1]
    let $target := map:get($result-map, "target")
    return
        $target
};

declare
    %test:assertTrue
function t:paraphrases-one-line()
as xs:boolean {
    let $cit := <cit xmlns="http://www.tei-c.org/ns/1.0" type="paraphrase">
          <quote>The fifth dawn was breaking.</quote>
          <note type="parallel-text">
            <note type="original-phrase">And evening passed and morning came, marking the fifth day.</note>
            <bibl type="provided-by-editor" source="#item-in-listBibl">Gen 1,23.</bibl>
          </note>
        </cit>
    let $result := edit:make-paraphrase-text($cit)
    return
        $result = """The fifth dawn was breaking."": a paraphrase of ""And evening passed and morning came, marking the fifth day."" (Gen 1,23.)"
};

declare 
    %test:assertTrue
function t:paraphrase-several-lines-last() {
    let $lines := <body xmlns="http://www.tei-c.org/ns/1.0">
                    <ab>
                        <cit type="paraphrase" next="#prov5.spr66.2">
                            <quote next="#prov5.1.spr66">this is a paraphrase</quote>
                        </cit>
                    </ab>
                    <ab>
                        <cit type="paraphrase" xml:id="prov5.spr66.2"  next="#prov5.spr66.3">
                            <quote next="#prov5.1.spr66">that is continued</quote>
                        </cit>
                    </ab>
                    <ab>
                        <cit type="paraphrase" xml:id="prov5.spr66.3">
                            <quote xml:id="prov5.1.spr66">in another line</quote>
                            <note type="parallel-text">
                                <note type="original-phrase">the original text of the paraphrase</note>
                                <bibl type="provided-by-editor" source="#Peshitta_Prov_Eccl_etc">Prov 27,20.</bibl>
                            </note>
                        </cit>
                    </ab>
                </body>
    let $result := edit:make-paraphrase-text($lines//tei:ab[3]/tei:cit)
    return
        $result = """this is a paraphrase that is continued in another line"": a paraphrase of ""the original text of the paraphrase"" (Prov 27,20.)"
};

declare
    %test:assertEquals("#1,#2,#3")
function t:paraphrase-several-lines-target-last() {
    let $lines := <body xmlns="http://www.tei-c.org/ns/1.0">
                    <text xml:lang="karshuni">
                    <pb n="1a"/>
                        <ab>
                            <cit type="paraphrase" next="#prov5.spr66.2" id="1">
                                <quote next="#prov5.1.spr66">this is a paraphrase</quote>
                            </cit>
                        </ab>
                        <ab>
                            <cit type="paraphrase" xml:id="prov5.spr66.2"  next="#prov5.spr66.3" id="2">
                                <quote next="#prov5.1.spr66">that is continued</quote>
                            </cit>
                        </ab>
                        <ab>
                            <cit type="paraphrase" xml:id="prov5.spr66.3" id="3">
                                <quote xml:id="prov5.1.spr66">in another line</quote>
                                <note type="parallel-text">
                                    <note type="original-phrase">the original text of the paraphrase</note>
                                    <bibl type="provided-by-editor" source="#Peshitta_Prov_Eccl_etc">Prov 27,20.</bibl>
                                </note>
                            </cit>
                        </ab>
                    </text>
                </body>
    let $result :=  edit:get-target-information($lines//tei:ab[3]/tei:cit, "12345", "M987654321")
    return
        map:get($result, "selector") => map:get("value")
};

declare 
    %test:assertTrue
function t:paraphrase-several-lines-last-several-parallels() {
    let $lines := <body xmlns="http://www.tei-c.org/ns/1.0">
                    <ab>
                        <cit type="paraphrase" next="#prov5.spr66.2">
                            <quote next="#prov5.1.spr66">this is a paraphrase</quote>
                        </cit>
                    </ab>
                    <ab>
                        <cit type="paraphrase" xml:id="prov5.spr66.2"  next="#prov5.spr66.3">
                            <quote next="#prov5.1.spr66">that is continued</quote>
                        </cit>
                    </ab>
                    <ab>
                        <cit type="paraphrase" xml:id="prov5.spr66.3">
                            <quote xml:id="prov5.1.spr66">in another line</quote>
                            <note type="parallel-text">
                                <note type="original-phrase">the original text #1</note>
                                <bibl type="provided-by-editor" source="#Peshitta_Prov_Eccl_etc">Gen 1,1</bibl>
                            </note>
                            <note type="parallel-text">
                                <note type="original-phrase">the original text #2</note>
                                <bibl type="provided-by-editor" source="#Peshitta_Prov_Eccl_etc">Eccl 1,1</bibl>
                            </note>
                            <note type="parallel-text">
                                <note type="original-phrase">the original text #3</note>
                                <bibl type="provided-by-editor" source="#Peshitta_Prov_Eccl_etc">Prov 1,1</bibl>
                            </note>
                        </cit>
                    </ab>
                </body>
    let $result := edit:make-paraphrase-text($lines//tei:ab[3]/tei:cit)
    return
        $result = """this is a paraphrase that is continued in another line"": a paraphrase of the following parallel texts: ""the original text #1"" (Gen 1,1), ""the original text #2"" (Eccl 1,1), ""the original text #3"" (Prov 1,1)"
};

declare 
    %test:assertTrue
function t:names-over-two-lines()
as xs:boolean {
    let $page := <TEI xmlns="http://www.tei-c.org/ns/1.0">
            <text>
                <group>
                    <text xml:lang="syc">
                        <body xml:lang="syc">
                            <pb id="N1l4l2l4l4l759" n="10b" facs="textgrid:3vwb8"/>
                            <ab id="N1l4l2l4l4l763">
                                <persName id="N1l4l2l4l4l763l4" next="#mingana_10b_2">
                                    <w xml:id="t_433_N1l5l3l5l5l764l5l3_w_1" type="token">ܕܣܲܪ</w>
                                </persName>
                            </ab>
                            <ab id="N1l4l2l4l4l765">
                                <lb id="N1l4l2l4l4l765l1" break="no"/>
                                <persName id="N1l4l2l4l4l765l2" xml:id="mingana_10b_2">
                                    <w xml:id="t_433_N1l5l3l5l5l766l3l3_w_1" type="token">ܚܲܕܘܼܡ</w>
                                </persName>
                            </ab>
                            <pb id="N1l4l2l4l4l801" n="11a" facs="textgrid:3vwb9"/>
                        </body>
                    </text>
                </group>
            </text>
        </TEI>
    let $result := edit:get-annotations($page, "3r8m5")
    return
        (
            count($result) = 1
            and map:get($result, "body") => map:get("value") = "ܕܣܲܪܚܲܕܘܼܡ"
            and map:get($result, "target") => array:get(1) 
                => map:get("selector") => map:get("value") = "#N1l4l2l4l4l763l4,#N1l4l2l4l4l765l2"
        )
};

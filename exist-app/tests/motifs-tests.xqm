xquery version "3.1";

module namespace t="http://ahiqar.uni-goettingen.de/ns/annotations/motifs/tests";

declare namespace http = "http://expath.org/ns/http-client";
declare namespace tei="http://www.tei-c.org/ns/1.0";

import module namespace commons="http://ahiqar.uni-goettingen.de/ns/commons" at "../modules/commons.xqm";
import module namespace me="http://ahiqar.uni-goettingen.de/ns/motifs-expansion" at "/db/apps/ahiqar/modules/motifs-expansion.xqm";
import module namespace motifs="http://ahiqar.uni-goettingen.de/ns/annotations/motifs" at "../modules/AnnotationAPI/motifs.xqm";
import module namespace tc="http://ahiqar.uni-goettingen.de/ns/tests/commons" at "test-commons.xqm";
import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";

declare variable $t:sample-doc := doc($commons:data || "/sample_teixml.xml");

declare
    %test:assertEquals("1")
function t:get-motifs()
as xs:integer {
    let $teixml-uri := "sample_teixml"
    let $page := "82a"
    let $pages := commons:get-page-fragments($teixml-uri, $page)
    return
        motifs:get-motifs($pages, $teixml-uri)
        => count()
};

declare
    %test:assertTrue
function t:is-annotationPage-endpoint-http200()
as xs:boolean {
    let $url := $tc:server || "/annotations/ahiqar/syriac/sample_edition/82a/annotationPage.json"
    return
        tc:is-endpoint-http200($url)
};

declare
    (: check if all parts are present.
     : no further tests are needed since the content has been tested while testing
     : the underlying function. :)
    %test:assertXPath("map:get($result, 'x-content-type') = 'Motif'")
    %test:assertXPath("map:get($result, 'value') = 'Successful courtier'")
    %test:assertXPath("map:get($result, 'format') = 'text/plain'")
    %test:assertXPath("map:get($result, 'type') = 'TextualBody'")
function t:does-annotationPage-have-motifs()
as item() {
    let $url := $tc:server || "/annotations/ahiqar/syriac/sample_edition/82a/annotationPage.json"
    let $req := <http:request href="{$url}" method="get">
                        <http:header name="Connection" value="close"/>
                   </http:request>
    let $items := 
        http:send-request($req)[2]
        => util:base64-decode()
        => parse-json()
        => map:get("annotationPage")
        => map:get("items")
    return
        $items?(array:size($items))
        => map:get("body")
};

declare
    %test:assertEquals("Parable (plants)")
function t:get-body-value()
as xs:string {
    let $motif := <span xmlns="http://www.tei-c.org/ns/1.0" id="N1.4.2.4.4.8.4-1" type="motif" n="parable_plants" next="#N1.4.2.4.4.8.4-2"/>
    return
        motifs:get-body-value($motif)
};

declare
    %test:assertXPath("map:get($result, 'format') = 'text/plain'")
    %test:assertXPath("map:get($result, 'type') = 'TextualBody'")
    %test:assertXPath("map:get($result, 'value') = 'Parable (plants)'")
    %test:assertXPath("map:get($result, 'x-content-type') = 'Motif'")
function t:get-body-object()
as map(*) {
    let $motif := <span xmlns="http://www.tei-c.org/ns/1.0" id="N1.4.2.4.4.8.4-1" type="motif" n="parable_plants" next="#N1.4.2.4.4.8.4-2"/>
    return
        motifs:get-body-object($motif)
};

declare
    %test:assertXPath("map:get($result, 'format') = 'text/xml'")
    %test:assertXPath("map:get($result, 'id') = 'http://ahiqar.uni-goettingen.de/ns/annotations/sample_teixml/N1.4.2.4.4.8.4-1'")
    %test:assertXPath("map:get($result, 'language') = 'karshuni'")
function t:get-target-information()
as map(*) {
    let $teixml-uri := "sample_teixml"
    let $tei-xml-base-uri := $commons:data || "sample_teixml.xml"
    let $doc := commons:get-page-fragment($tei-xml-base-uri, "82a", "transcription")
    let $motif := $doc/descendant::tei:span[@type = "motif"][1]
    return
        motifs:get-target-information($teixml-uri, $motif)
};

declare
    %test:assertXPath("map:get($result, 'target) => map:get('selector') => map:get('value') = '#t_Add_2020_MD16803N1l5l3l5l5l9l6_1,#t_Add_2020_MD16803N1l5l3l5l5l11l2_1,#t_Add_2020_MD16803N1l5l3l5l5l11l2_2,#t_Add_2020_MD16803N1l5l3l5l5l11l2_3,#t_Add_2020_MD16803N1l5l3l5l5l11l2_4,#t_Add_2020_MD16803N1l5l3l5l5l11l2_5'")
function t:get-motifs-css-value-summary-start-and-end-on-page() {
    let $teixml-uri := "sample_teixml"
    let $page := "82a"
    let $pages := commons:get-page-fragments($teixml-uri, $page)
    return
        motifs:get-motifs($pages, $teixml-uri)
};

declare
    %test:assertXPath("map:get($result, 'target) => map:get('selector') => map:get('value') = '#t_Add_2020_MD16803N1l5l3l5l5l9l6_1'")
function t:get-motifs-css-value-summary-start-on-page() {
    let $teixml-uri := "sample_teixml"
    let $page := "82a"
    let $page :=
        <TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader id="MD16306N1l2">
              <fileDesc id="MD16306N1l2l2">
                 <titleStmt id="MD16306N1l2l2l2">
                    <title id="MD16306N1l2l2l2l2" type="main">The Proverbs or History of Aḥīḳar the wise, the scribe of Sanḥērībh,
                       king of Assyria and Nineveh</title>
                    <editor id="MD16306N1l2l2l2l4">Simon Birol</editor>
                    <editor id="MD16306N1l2l2l2l6">Aly Elrefaei</editor>
                 </titleStmt>
                 <publicationStmt id="MD16306N1l2l2l4">
                    <authority id="MD16306N1l2l2l4l2">Georg-August Universität Göttingen, Theologische Fakultät, Seminar für Altes
                       Testament</authority>
                    <availability id="MD16306N1l2l2l4l4">
                       <licence id="MD16306N1l2l2l4l4l2" target="https://creativecommons.org/licenses/by-sa/4.0/">Distributed under a Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) License.</licence>
                    </availability>
                 </publicationStmt>
                 <sourceDesc id="MD16306N1l2l2l6">
                    <msDesc id="MD16306N1l2l2l6l2">
                       <msIdentifier id="MD16306N1l2l2l6l2l2">
                          <settlement id="MD16306N1l2l2l6l2l2l2">
                             <country id="MD16306N1l2l2l6l2l2l2l2">Great Britain</country>
                          </settlement>
                          <institution id="MD16306N1l2l2l6l2l2l4">University of Cambridge - Cambridge University Library</institution>
                          <idno id="MD16306N1l2l2l6l2l2l6">Add. 2020</idno>
                       </msIdentifier>
                       <msContents id="MD16306N1l2l2l6l2l4">
                          <msItem id="MD16306N1l2l2l6l2l4l2">
                             <textLang id="MD16306N1l2l2l6l2l4l2l2">Classical Syriac</textLang>
                          </msItem>
                       </msContents>
                       <history id="MD16306N1l2l2l6l2l6">
                          <origin id="MD16306N1l2l2l6l2l6l2">
                             <objectType id="MD16306N1l2l2l6l2l6l2l2">codex</objectType>
                             <country id="MD16306N1l2l2l6l2l6l2l4">Iraq</country>
                             <placeName id="MD16306N1l2l2l6l2l6l2l6">Alqosh</placeName>
                             <date id="MD16306N1l2l2l6l2l6l2l8" precision="high">18.10.1697</date>
                          </origin>
                       </history>
                    </msDesc>
                    <listBibl id="MD16306N1l2l2l6l4">
                       <bibl id="MD16306N1l2l2l6l4l2">Wright, William: A catalogue of the Syriac manuscripts preserved in the library
                          of the University of Cambridge, vol. 2, Cambridge 1901, p. 583-9.</bibl>
                    </listBibl>
                 </sourceDesc>
              </fileDesc>
           </teiHeader>
            <text>
                <group>
                    <text id="MD16306N1l4l2l4" xml:lang="karshuni" type="transcription">
                        <body id="MD16306N1l4l2l4l4">
                            <pb id="MD16306N1l4l2l4l4l2" facs="textgrid:3r1nz" n="82a" xml:id="a1"/>
                            <ab id="MD16306N1l4l2l4l4l8">
                                <w xml:id="t_Add_2020_MD16803N1l5l3l5l5l9l4_1" type="token">الفارسي.</w>
                                <successful_courtier xmlns="http://ahiqar.uni-goettingen.de/ns/motifs" id="MD16306N1l4l2l4l4l8l4" type="start" md5="md595117980d6"/>
                                <w xml:id="t_Add_2020_MD16803N1l5l3l5l5l9l6_1" type="token">وكان</w>
                            </ab>
                           <pb id="MD16306N1l4l2l4l4l3" facs="textgrid:3r1nv" n="82b" xml:id="a2"/>
                        </body>
                    </text>
                </group>
            </text>
        </TEI>
    return
        motifs:get-motifs($pages, $teixml-uri)
};

declare
    %test:assertXPath("map:get($result, 'target) => map:get('selector') => map:get('value') = '#t_Add_2020_MD16803N1l5l3l5l5l11l2_1,#t_Add_2020_MD16803N1l5l3l5l5l11l2_2,#t_Add_2020_MD16803N1l5l3l5l5l11l2_3,#t_Add_2020_MD16803N1l5l3l5l5l11l2_4,#t_Add_2020_MD16803N1l5l3l5l5l11l2_5'")
function t:get-motifs-css-value-summary-end-on-page() {
    let $teixml-uri := "sample_teixml"
    let $page := "82a"
    let $page :=
        <TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader id="MD16306N1l2">
              <fileDesc id="MD16306N1l2l2">
                 <titleStmt id="MD16306N1l2l2l2">
                    <title id="MD16306N1l2l2l2l2" type="main">The Proverbs or History of Aḥīḳar the wise, the scribe of Sanḥērībh,
                       king of Assyria and Nineveh</title>
                    <editor id="MD16306N1l2l2l2l4">Simon Birol</editor>
                    <editor id="MD16306N1l2l2l2l6">Aly Elrefaei</editor>
                 </titleStmt>
                 <publicationStmt id="MD16306N1l2l2l4">
                    <authority id="MD16306N1l2l2l4l2">Georg-August Universität Göttingen, Theologische Fakultät, Seminar für Altes
                       Testament</authority>
                    <availability id="MD16306N1l2l2l4l4">
                       <licence id="MD16306N1l2l2l4l4l2" target="https://creativecommons.org/licenses/by-sa/4.0/">Distributed under a Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) License.</licence>
                    </availability>
                 </publicationStmt>
                 <sourceDesc id="MD16306N1l2l2l6">
                    <msDesc id="MD16306N1l2l2l6l2">
                       <msIdentifier id="MD16306N1l2l2l6l2l2">
                          <settlement id="MD16306N1l2l2l6l2l2l2">
                             <country id="MD16306N1l2l2l6l2l2l2l2">Great Britain</country>
                          </settlement>
                          <institution id="MD16306N1l2l2l6l2l2l4">University of Cambridge - Cambridge University Library</institution>
                          <idno id="MD16306N1l2l2l6l2l2l6">Add. 2020</idno>
                       </msIdentifier>
                       <msContents id="MD16306N1l2l2l6l2l4">
                          <msItem id="MD16306N1l2l2l6l2l4l2">
                             <textLang id="MD16306N1l2l2l6l2l4l2l2">Classical Syriac</textLang>
                          </msItem>
                       </msContents>
                       <history id="MD16306N1l2l2l6l2l6">
                          <origin id="MD16306N1l2l2l6l2l6l2">
                             <objectType id="MD16306N1l2l2l6l2l6l2l2">codex</objectType>
                             <country id="MD16306N1l2l2l6l2l6l2l4">Iraq</country>
                             <placeName id="MD16306N1l2l2l6l2l6l2l6">Alqosh</placeName>
                             <date id="MD16306N1l2l2l6l2l6l2l8" precision="high">18.10.1697</date>
                          </origin>
                       </history>
                    </msDesc>
                    <listBibl id="MD16306N1l2l2l6l4">
                       <bibl id="MD16306N1l2l2l6l4l2">Wright, William: A catalogue of the Syriac manuscripts preserved in the library
                          of the University of Cambridge, vol. 2, Cambridge 1901, p. 583-9.</bibl>
                    </listBibl>
                 </sourceDesc>
              </fileDesc>
           </teiHeader>
            <text>
                <group>
                    <text id="MD16306N1l4l2l4" xml:lang="karshuni" type="transcription">
                        <body id="MD16306N1l4l2l4l4">
                            <pb id="MD16306N1l4l2l4l4l2" facs="textgrid:3r1nz" n="82a" xml:id="a1"/>
                            <ab id="MD16306N1l4l2l4l4l10">
                                <w xml:id="t_Add_2020_MD16803N1l5l3l5l5l11l2_1" type="token">كاتب</w>
                                <w xml:id="t_Add_2020_MD16803N1l5l3l5l5l11l2_2" type="token">الملك</w>
                                <w xml:id="t_Add_2020_MD16803N1l5l3l5l5l11l2_3" type="token">ومتقدم</w>
                                <w xml:id="t_Add_2020_MD16803N1l5l3l5l5l11l2_4" type="token">عنده</w>
                                <w xml:id="t_Add_2020_MD16803N1l5l3l5l5l11l2_5" type="token">جدًا</w>
                                <successful_courtier xmlns="http://ahiqar.uni-goettingen.de/ns/motifs" id="MD16306N1l4l2l4l4l10l2" type="end" md5="md595117980d6"/>
                                <w xml:id="t_Add_2020_MD16803N1l5l3l5l5l11l4_1" type="token">.</w>
                            </ab>
                           <pb id="MD16306N1l4l2l4l4l3" facs="textgrid:3r1nv" n="82b" xml:id="a2"/>
                        </body>
                    </text>
                </group>
            </text>
        </TEI>
    return
        motifs:get-motifs($pages, $teixml-uri)
};

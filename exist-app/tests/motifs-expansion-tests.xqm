xquery version "3.1";

module namespace met="http://ahiqar.uni-goettingen.de/ns/motifs-expansion/tests";

declare namespace tei="http://www.tei-c.org/ns/1.0";

import module namespace me="http://ahiqar.uni-goettingen.de/ns/motifs-expansion" at "../modules/motifs-expansion.xqm";
import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";

declare
    %test:assertTrue
function met:simple() {
    let $tei :=
        <TEI xmlns="http://www.tei-c.org/ns/1.0">
            <text>
                <body>
                    <ab>foo</ab>
                    <?oxy_comment_start comment="bla" ?>
                    <ab>bar</ab>
                    <pb n="1" />
                    <ab>foo</ab>
                    <?oxy_comment_end ?>
                    <ab>bar</ab>
                </body>
            </text>
        </TEI>
    let $reference :=
        <TEI xmlns="http://www.tei-c.org/ns/1.0">
            <text>
                <body>
                    <ab>foo</ab>
                    <bla xmlns="http://ahiqar.uni-goettingen.de/ns/motifs" type="start" md5="5ae8d8b14ff7abb878e30425c869905a"/>
                    <ab>bar</ab>
                    <pb n="1"/>
                    <ab>foo</ab>
                    <bla xmlns="http://ahiqar.uni-goettingen.de/ns/motifs" type="end" md5="5ae8d8b14ff7abb878e30425c869905a"/>
                    <ab>bar</ab>
                </body>
            </text>
        </TEI>
    let $result := me:main($tei)

    return
        $result = $reference
};

declare
    %test:assertTrue
function met:encapsulated() {
    let $tei :=
        <TEI xmlns="http://www.tei-c.org/ns/1.0">
            <text>
                <body>
                    <?oxy_comment_start ts="123" comment="blubb" ?>
                    <ab>foo</ab>
                    <?oxy_comment_start comment="quark" ?>
                    <ab>quark</ab>
                    <?oxy_comment_start comment="bla" ?>
                    <ab>bar</ab>
                    <pb n="1" />
                    <?oxy_comment_end ?>
                    <ab>foo</ab>
                    <?oxy_comment_end ?>
                    <ab>bar</ab>
                    <?oxy_comment_end ?>
                </body>
            </text>
        </TEI>

    let $reference :=
        <TEI xmlns="http://www.tei-c.org/ns/1.0">
            <text>
                <body>
                    <blubb xmlns="http://ahiqar.uni-goettingen.de/ns/motifs" type="start" md5="010d1ccb9e7218a24315cfea42409881"/>
                    <ab>foo</ab>
                    <quark xmlns="http://ahiqar.uni-goettingen.de/ns/motifs" type="start" md5="3c86672d03da7ccf1dea3b18fc737d0a"/>
                    <ab>quark</ab>
                    <bla xmlns="http://ahiqar.uni-goettingen.de/ns/motifs" type="start" md5="5ae8d8b14ff7abb878e30425c869905a"/>
                    <ab>bar</ab>
                    <pb n="1"/>
                    <bla xmlns="http://ahiqar.uni-goettingen.de/ns/motifs" type="end" md5="5ae8d8b14ff7abb878e30425c869905a"/>
                    <ab>foo</ab>
                    <quark xmlns="http://ahiqar.uni-goettingen.de/ns/motifs" type="end" md5="3c86672d03da7ccf1dea3b18fc737d0a"/>
                    <ab>bar</ab>
                    <blubb xmlns="http://ahiqar.uni-goettingen.de/ns/motifs" type="end" md5="010d1ccb9e7218a24315cfea42409881"/>
                </body>
            </text>
        </TEI>
        
    let $result := me:main($tei)
    
    return
        $result = $reference
};

declare function local:prepare-for-comparison($nodes as node())
as xs:string {
    serialize($nodes)
    => replace("[\t\r\n]", "")
    => replace("\s+", " ")
};
